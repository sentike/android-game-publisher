﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvnTest
{
    class Program
    {
        public static SharpSvn.SvnClient SvnClient { get; } = new SharpSvn.SvnClient();
        public static string WorkingDirectory { get; set; } = @"C:\Games\GunsOfBoomClone\WindowsServer";

        public static string TrunkBranch { get; set; } = "http://WIN-12782K5KS1B:5500/svn/LokaAWGameServers/trunk";
        public static string TargetBranch { get; set; } = "http://WIN-12782K5KS1B:5500/svn/LokaAWGameServers/branches/Server_195";

        static void Main(string[] args)
        {
            SvnClient.GetInfo(SharpSvn.SvnTarget.FromString(TrunkBranch), out var SvnInfo);

            //SvnClient.GetStatus(WorkingDirectory, out var SvnStatus);

            SvnClient.CleanUp(WorkingDirectory);
            //SvnClient.DiffMerge(WorkingDirectory, SharpSvn.SvnTarget.FromString(TrunkBranch), SharpSvn.SvnTarget.FromString(TargetBranch), new SharpSvn.SvnDiffMergeArgs { Force = true });


            Console.WriteLine($"Merge begin, Revision: {SvnInfo.Revision}");
            var MergeRange = new SharpSvn.SvnRevisionRange(SharpSvn.SvnRevision.Head, SvnInfo.Revision);
            var ExecuteStatus = SvnClient.Merge
            (
                WorkingDirectory,
                TrunkBranch,
                SharpSvn.SvnRevisionRange.FromRevision(SvnInfo.Revision),
                new SharpSvn.SvnMergeArgs { Force = true/*, DryRun = true, CheckForMixedRevisions = false */}
            );
            Console.WriteLine("Merge end");

            SvnClient.Commit(WorkingDirectory);

            //foreach(var f in SvnStatus)
            //{
            //    Console.WriteLine($"{f.Path} - {f.LocalNodeStatus} / {f.LocalContentStatus}");
            //}
            Console.ReadKey();
        }
    }
}
