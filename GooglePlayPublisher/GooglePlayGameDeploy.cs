﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidGamePublisher.Extensions;
using Google.Apis.AndroidPublisher.v3;
using Google.Apis.AndroidPublisher.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using NLog;

namespace AndroidGamePublisher.Deploy.Game
{
    public class GooglePlayGameDeploy
    {
        public GooglePlayCredential CredentialManifest { get; set; }
        private ServiceAccountCredential Credential { get; set; }
        private AndroidPublisherService PublisherService { get; set; }
        private EditsResource Edits { get; set; }

        private static ILogger Logger { get; } = LogManager.GetCurrentClassLogger();

        public string DeployTrack { get; set; }
        public string PackageName { get; set; }
        private string TransactionId { get; set; }

        public string ApkFileLocation { get; set; }
        public string ObbFileLocation { get; set; }

        public int GameStoreVersion { get; set; }

        private void Authorization()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Credential loading ...");

            //==============================================================
            Credential = new ServiceAccountCredential
            (
                new ServiceAccountCredential.Initializer(CredentialManifest.ServiceAccountEmail)
                {
                    Scopes = new[] { AndroidPublisherService.Scope.Androidpublisher }
                }.FromCertificate(CredentialManifest.Certificate)
            );

            //==============================================================
            Logger.Debug($"[GooglePlay] Credential loaded");
        }

        private void StartAndroidPublisherService()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp start client ...");

            //==============================================================
            PublisherService = new AndroidPublisherService(new BaseClientService.Initializer
            {
                ApiKey = CredentialManifest.ApiKey,
                ApplicationName = CredentialManifest.ApplicationName,
                HttpClientInitializer = Credential,
            });

            //==============================================================
            Logger.Debug($"[GooglePlay] Start client successfully");
        }

        private void StartEditTransaction()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp create EditsResource ...");
            Edits = PublisherService.Edits;
            Logger.Debug($"[GooglePlay] Create EditsResource successfully");

            //==============================================================
            Logger.Info($"[GooglePlay] Attemp start Transaction ...");
            var Transaction = Edits.Insert(null, PackageName).Execute();
            Logger.Debug($"[GooglePlay] start Transaction successfully");

            //==============================================================
            TransactionId = Transaction.Id;

            //==============================================================
            Logger.Info($"[GooglePlay] TransactionId: {TransactionId}");

        }

        private void CommitChanges()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp Commit changes ...");

            //==============================================================
            Edits.Commit(PackageName, TransactionId).Execute();

            //==============================================================
            Logger.Debug($"[GooglePlay] Commit changes successfully");
        }

        private void PublishProxy()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] PackageName: {PackageName}");
            Logger.Info($"[GooglePlay] GameStoreVersion: {GameStoreVersion}");

            //==============================================================
            Authorization();

            //==============================================================
            StartAndroidPublisherService();

            //==============================================================
            StartEditTransaction();

            //==============================================================
            PublishApk();
            PublishObb();

            //==============================================================
            SwitchTracks();

            //==============================================================
            CommitChanges();
        }

        public void Publish()
        {
            Logger.Warn($"============================================================");
            Logger.Info($"[GooglePlay] Prepare to GooglePlay publish : {DeployTrack} ...");

            //==============================================================
            try
            {
                PublishProxy();
            }
            catch(Exception e)
            {         
                //==============================================================
                Logger.Error($"============================================================");
                Logger.Error($"[GooglePlay] Failed to publish the application to Google Play");
                Logger.Error($"[GooglePlay] Publish APK and OBB manually");
                Logger.Error($"============================================================");

                //==============================================================
                throw e;
            }
        }

        private void GetApkList()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp get APK list ...");

            //==============================================================
            var Response = Edits.Apks.List(PackageName, TransactionId).Execute();

            //==============================================================
            Logger.Info($"[GooglePlay] Getted APK list: {Response.Apks.Count}");

            //==============================================================
            foreach(var apk in Response.Apks)
            {
                Logger.Debug($"> {apk.VersionCode} | Sha1: {apk.Binary.Sha1} | ETag: {apk.ETag}");
            }
        }

        private void GetTrackList()
        {       
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp get Track list ...");

            //==============================================================
            var Response = Edits.Tracks.List(PackageName, TransactionId).Execute();

            //==============================================================
            Logger.Info($"[GooglePlay] Getted Track list: {Response.Tracks.Count}");

            //==============================================================
            foreach (var track in Response.Tracks)
            {
                var releases = track.Releases.Select(p => $"{p.Name} -> {p.Status}").ToArray();
                Logger.Debug($"> {track.TrackValue} | ETag: {track.ETag} | {string.Join(",", releases)}");
            }
        }


        private void PublishApk()
        {
            GetApkList();
            GetTrackList();

            Logger.Info($"[GooglePlay] Attemp read APK file ...");
            Logger.Info($"[GooglePlay] ApkFileLocation: {ApkFileLocation}");

            using (var fs = new FileStream(ApkFileLocation, FileMode.Open))
            {
                Logger.Warn($"[GooglePlay] Attemp upload APK file ...");

                var UploadRequest = Edits.Apks.Upload
                (
                    PackageName,
                    TransactionId,
                    fs,
                    "application/vnd.android.package-archive"
                );

                UploadRequest.ProgressChanged += progress => OnUploadProgressChanged("APK", progress, fs.Length);
                UploadRequest.UploadAsync().Wait();

                Logger.Debug($"[GooglePlay] Upload APK file successfully");
            }
        }

        private void UploadObb()
        {
            Logger.Info($"[GooglePlay] Attemp read OBB file ...");
            Logger.Info($"[GooglePlay] ObbFileLocation: {ObbFileLocation}");

            using (var fs = new FileStream(ObbFileLocation, FileMode.Open))
            {
                Logger.Warn($"[GooglePlay] Attemp upload OBB file ...");

                var UploadRequest = Edits.Expansionfiles.Upload
                (
                    PackageName,
                    TransactionId,
                    GameStoreVersion,
                    EditsResource.ExpansionfilesResource.UploadMediaUpload.ExpansionFileTypeEnum.Main, fs,
                    "application/octet-stream"
                );

                UploadRequest.ProgressChanged += progress => OnUploadProgressChanged("OBB", progress, fs.Length);
                UploadRequest.UploadAsync().Wait();

                Logger.Debug($"[GooglePlay] Upload OBB file successfully");
            }
        }

        private void OnUploadProgressChanged(string action, IUploadProgress progress, long total)
        {
            if (progress.Status == UploadStatus.Completed)
            {
                Logger.Debug($"> [{action}] {progress.Status} | BytesSent: {progress.BytesSent} of {total}");
            }
            else if (progress.Status == UploadStatus.Failed)
            {
                Logger.WriteExceptionMessage($"> [{action}] {progress.Status} | BytesSent: {progress.BytesSent}", progress.Exception);
            }
            else
            {
                Logger.Warn($"> [{action}] {progress.Status} | BytesSent: {progress.BytesSent} of {total} | {progress.BytesSent * 100 / total}%");
            }
        }

        private void PublishObb()
        {
            UploadObb();
        }


        private void SwitchTracks()
        {
            //==============================================================
            var message = new TrackRelease();

            //==============================================================
            Logger.Info($"[GooglePlay] Attemp switch Tracks to {DeployTrack} | GameStoreVersion: {GameStoreVersion} ...");

            //======================================
            message.Name = $"{DeployTrack} v{GameStoreVersion}";
            message.Status = "completed";
            message.VersionCodes = new List<long?> { GameStoreVersion };
            message.ReleaseNotes = new List<LocalizedText>
            {
                new LocalizedText
                {
                    Language = "ru-RU",
                    Text = $"Автоматическое обновление v{GameStoreVersion} от {DateTime.Now}"
                },
                new LocalizedText
                {
                    Language = "en-US",
                    Text = $"Automation bild v{GameStoreVersion} at {DateTime.Now}"
                }
            };

            var track = new Track()
            {
                Releases = new List<TrackRelease>() { message }
            };

            Edits.Tracks.Update(track, PackageName, TransactionId, DeployTrack).Execute();
            Logger.Debug($"[GooglePlay] Switch Tracks to {DeployTrack} successfully");

        }
    }
}
