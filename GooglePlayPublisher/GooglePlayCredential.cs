﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using AndroidGamePublisher.Extensions;
using NLog;

namespace AndroidGamePublisher.Deploy
{
    public class GooglePlayCredential
    {
        //======================================
        public string PackageName { get; set; } 
        public string ServiceAccountEmail { get; set; } 

        //======================================
        public string ApiKey { get; set; }
        public string ApplicationName { get; set; }

        //======================================
        public string SafeCertificatePath => CertificatePath;

        public string CertificatePath { get; set; }
        public string CertificatePassword { get; set; } = "notasecret";



        //======================================
        //[JsonIgnore]
        public X509Certificate2 Certificate { get; set; }

        public void Init()
        {
            //===========================================================
            byte[] RawFile = null;

            //===========================================================
            {
                //--------------------------------------------
                if (string.IsNullOrWhiteSpace(PackageName))
                {
                    throw new ArgumentNullException(nameof(PackageName), "PackageName was empty");
                }

                //--------------------------------------------
                if (string.IsNullOrWhiteSpace(ServiceAccountEmail))
                {
                    throw new ArgumentNullException(nameof(ServiceAccountEmail), "ServiceAccountEmail was empty");
                }

                //--------------------------------------------
                if (string.IsNullOrWhiteSpace(ApiKey))
                {
                    throw new ArgumentNullException(nameof(ApiKey), "ApiKey was empty");
                }

                //--------------------------------------------
                if (string.IsNullOrWhiteSpace(ApplicationName))
                {
                    throw new ArgumentNullException(nameof(ApplicationName), "ApplicationName was empty");
                }
            }

            //===========================================================
            {
                //--------------------------------------------
                if (string.IsNullOrWhiteSpace(CertificatePath))
                {
                    throw new ArgumentNullException(nameof(CertificatePath), "CertificatePath was empty");
                }

                //--------------------------------------------
                if (string.IsNullOrWhiteSpace(CertificatePassword))
                {
                    throw new ArgumentNullException(nameof(CertificatePassword), "ServiceAccountEmail was empty");
                }

                //--------------------------------------------
                if (File.Exists(SafeCertificatePath) == false)
                {
                    throw new FileNotFoundException("Certificate not found", SafeCertificatePath);
                }

                //--------------------------------------------
                try
                {
                    RawFile = File.ReadAllBytes(SafeCertificatePath);
                }
                catch (Exception e)
                {
                    throw new FileLoadException("Failed read Certificate file #1", SafeCertificatePath, e);
                }

                //--------------------------------------------
                if (RawFile == null || RawFile.Length < 8)
                {
                    throw new FileLoadException($"Failed read Certificate file. Certificate was empty or invalid size: {RawFile?.Length}", SafeCertificatePath);
                }

                //--------------------------------------------
                try
                {
                    Certificate = new X509Certificate2(RawFile, CertificatePassword, X509KeyStorageFlags.Exportable);
                }
                 catch (Exception e)
                {
                    throw new FileLoadException("Failed read Certificate file #2", SafeCertificatePath, e);
                }

                if (Certificate.HasPrivateKey == false)
                {
                    throw new FileLoadException("Failed read Certificate file #3", SafeCertificatePath);
                }
            }

            //--------------------------------------------
        }

        public void Process()
        {
            //===================================================
            var Logger = LogManager.GetCurrentClassLogger();

            //===================================================
            Logger.Warn($"============================================================");
            Logger.Info($"PackageName: {PackageName}");
            Logger.Info($"ApplicationName: {ApplicationName}");
            Logger.Info($"ApiKey: {ApiKey}");
            Logger.Info($"ServiceAccountEmail: {ServiceAccountEmail}");

            Logger.Info($"CertificatePath: {CertificatePath}");
            Logger.Info($"CertificatePassword: {CertificatePassword}");

            Logger.Warn($"============================================================");

            //===================================================
            try
            {
                //-----------------------------------
                Init();

                //-----------------------------------
                Logger.Debug($"Certificate.FriendlyName: {Certificate.FriendlyName}");
                Logger.Debug($"Certificate.SerialNumber: {Certificate.SerialNumber}");
                Logger.Debug($"Certificate.Thumbprint: {Certificate.Thumbprint}");
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage("GooglePlayCredential", e, false);
            }

            Logger.Warn($"============================================================");
        }
    }
}
