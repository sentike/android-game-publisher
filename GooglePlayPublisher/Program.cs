﻿using AndroidGamePublisher.Deploy;
using AndroidGamePublisher.Deploy.Game;
using AndroidGamePublisher.Extensions;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GooglePlayPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            //======================================
            int ApkVersion = 0;

            //======================================
            var Credentials = new GooglePlayCredential();
            var DeployManager = new GooglePlayGameDeploy();

            //======================================
            Console.WriteLine("> Enter Apk version");
            var VersionStr = Console.ReadLine();

            //======================================
            while (int.TryParse(VersionStr, out ApkVersion) == false)
            {
                Console.WriteLine("! Bad apk version, try again");
            }


            //======================================
            try
            {

                Credentials.ApplicationName = "Loka AW";
                Credentials.PackageName = "com.VRSPro.LOKA_AW";
                Credentials.ServiceAccountEmail = "superuser@loka-aw-17856225.iam.gserviceaccount.com";
                Credentials.ApiKey = "AIzaSyBRU_LD0bELyhrX-m8rHAwXzFTj75toaak";
                Credentials.CertificatePassword = "notasecret";
                Credentials.CertificatePath = "application/LokaAW-739c849b9837.p12";
                Credentials.Process();
            }
            catch(Exception e)
            {
                Logger.WriteExceptionMessage("Credentials", e);
                Console.ReadKey();
                return;
            }

            //======================================
            try
            {
                //======================================
                DeployManager.CredentialManifest = Credentials;
                DeployManager.PackageName = Credentials.PackageName;
                DeployManager.DeployTrack = "internal";

                //======================================
                DeployManager.GameStoreVersion = ApkVersion;
                DeployManager.ApkFileLocation = "application/ShooterGame-Android-Shipping-armv7-es2.apk";
                DeployManager.ObbFileLocation = $"application/main.{DeployManager.GameStoreVersion}.com.VRSPro.LOKA_AW.obb";

                //======================================
                DeployManager.Publish();
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage("DeployManager", e);
            }

            //======================================
            Console.ReadKey();
        }

        private static ILogger Logger { get; } = LogManager.GetCurrentClassLogger();
    }
}
