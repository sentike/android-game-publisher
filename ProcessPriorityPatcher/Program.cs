﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessPriorityPatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            var listener = new ProcessListener();
            listener.Process();
        }
    }
}
