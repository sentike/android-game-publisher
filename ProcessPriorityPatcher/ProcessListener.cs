﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessPriorityPatcher
{
    public class ProcessListener
    {
        public Thread ProcessThread { get; set; }

        public void Start()
        {
            ProcessThread = new Thread(Process);
        }

        public void Process()
        {
            while (true)
            {
                ProcessProxy();
                Thread.Sleep(500);
            }
        }

        private void ProcessProxy()
        {
            //==========================
            var targets = new[]
            {
                "java",
                "clang++",
                "clang",
                "ld",
                "ar",
                "strip",
                "nm",

                "cl",
                "link",
                "UE4Editor-Cmd",
                "UE4Editor",
                "UnrealPak",
                "ShaderCompileWorker",
                "UnrealLightmass",
                "UnrealHeaderTool",
                "AutomationToolLauncher",
                
                "arm-linux-androideabi-strip",
                "arm-linux-androideabi-strings",
                "arm-linux-androideabi-ld",
                "arm-linux-androideabi-ranlib",
                "arm-linux-androideabi-objcopy",
                "arm-linux-androideabi-nm",
                "arm-linux-androideabi-size",
                "arm-linux-androideabi-ar",

                "UE4Game",
                "UE4Game-cmd",
                "UnrealBuildTool",
                "AutomationTool",

                
            };

            try
            {

                //==========================
                var avalible = System.Diagnostics.Process.GetProcesses();

                //==========================
                var process = avalible.Where(p => targets.Contains(p.ProcessName) && p.PriorityClass != ProcessPriorityClass.RealTime).ToArray();

                //==========================
                foreach (var proc in process)
                {
                    try
                    {
                        Console.WriteLine($"Try set {proc.ProcessName} / {proc.PriorityClass} priority");
                        proc.PriorityClass = ProcessPriorityClass.RealTime;
                        proc.PriorityBoostEnabled = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Failed set {proc.ProcessName} priority | Exception: {e.Message}");
                    }
                }
            }
            catch
            {

            }
        }
    }
}
