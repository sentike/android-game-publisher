﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidGamePublisher.Deploy;

namespace AndroidGamePublisher.Version
{
    public class ServerVersionModel
    {
        public int ServerVersionId { get; set; }
        public DeployTarget DeployTarget { get; set; }
        public bool DeprecatePreviewVersions { get; set; }
        public string AdminComment { get; set; }
    }

    public class GameVersionModel
    {
        public long StoreVersionId { get; set; }
        public long ServerVersionId { get; set; }
        public DeployTarget DeployTarget { get; set; }
        public bool DeprecatePreviewVersions { get; set; }
        public string AdminComment { get; set; }
    }
}
