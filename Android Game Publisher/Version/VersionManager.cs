﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AndroidGamePublisher.Deploy;
using AndroidGamePublisher.Extensions;
using NLog;

namespace AndroidGamePublisher.Version
{
    public enum Languages
    {
        Russian,
        English,
        Germany,
    }

    public class VersionManager
    {
        //====================================
        protected static ILogger Logger { get; } = LogManager.GetCurrentClassLogger();

        //====================================
        public VersionManager(DeployManifest manifest)
        {
            Manifest = manifest;
            IsAllowAutoIncrement = manifest.IsAllowAutoIncrement;
            ProjectPath = Path.GetDirectoryName(manifest.ProjectFilePath);
        }
        public DeployManifest Manifest { get; }

        //====================================
        public string ProjectPath { get; }
        public string ConfigDirectoryPath => Path.Combine(ProjectPath, "Config");
        public string DefaultEngineFilePath => Path.Combine(ConfigDirectoryPath, "DefaultEngine.ini");

        //====================================
        public string GooglePlayGameVersionFilePath => Path.Combine(ProjectPath, "Source", "ShooterGame", "GooglePlayGameVersion.h");

        //====================================
        public bool IsAllowAutoIncrement { get; }
        public int StoreVersion { get; private set; }
        public string VersionDisplayName { get; private set; }
        public DeployTarget GameDeployTarget { get; set; }
        public DeployTarget NextGameDeployTarget => Manifest?.DeployTarget ?? DeployTarget.None;

        private string ConfigFileContent { get; set; }

        public string DisplayVersion => Manifest?.DisplayVersion ?? "0.6";

        public int NextStoreVersion => StoreVersion + 1;
        public string NextVersionDisplayName => $"{DisplayVersion}.{NextStoreVersion}";
        public GameVersionModel NextGameVersionModel { get; private set; }
        public ServerVersionModel NextServerVersionModel { get; private set; }

        public int TargetServerVersion { get; set; }

        public int NextTargetServerVersion
        {
            get
            {
                if (Manifest?.IsAllowServerVersionIncrement ?? false)
                {
                    return TargetServerVersion + 1;
                }
                return TargetServerVersion;
            }
        }

        //====================================
        public bool Process()
        {
            try
            {
                ProcessProxy();
                return true;
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage("Failed Version Manager Process", e);
                return false;
            }
        }

        private Dictionary<Languages, string> DeployMessages { get; set; } = new Dictionary<Languages, string>();

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public string GetDeployMessage(Languages language)
        {
            if(DeployMessages.ContainsKey(language))
            {
                var Message = DeployMessages[language];
                return Message;
            }
            return string.Empty;
        }

        public Dictionary<Languages, string> GetDeployMessages(string InBase64Message)
        {
            try
            {
                //======================================
                var MessageStringContainer = Base64Decode(InBase64Message);

                //======================================
                var MessageDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<Languages, string>>(MessageStringContainer) ?? new Dictionary<Languages, string>
                {
                    { Languages.Russian, string.Empty },
                    { Languages.English, string.Empty },
                    { Languages.Germany, string.Empty },
                };

                //======================================
                return MessageDictionary;
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage("GetDeployMessages", e, false);
                return new Dictionary<Languages, string>
                {
                    { Languages.Russian, string.Empty },
                    { Languages.English, string.Empty },
                    { Languages.Germany, string.Empty },
                };
            }
        }



        private void ProcessProxy()
        {
            //====================================
            Logger.Info(new string('=', 48));

            //====================================
            DeployMessages = GetDeployMessages(DeployOptions.Options.Message);

            //====================================
            Logger.Info($"[Ru]: {GetDeployMessage(Languages.Russian)}");
            Logger.Info($"[En]: {GetDeployMessage(Languages.English)}");

            //====================================
            Logger.Info($"Preapre to load config file at {DefaultEngineFilePath}");
            LoadConfigFileContent();

            //====================================
            ParseStoreVersion();

            //====================================
            ParseTargetServerVersion();          

            //====================================
            ParseVersionDisplayName();

            //====================================
            ParseGameDeployTarget();

            //====================================
            if (IsAllowAutoIncrement == false)
            {
                Logger.Error($"IsAllowAutoIncrement was disabled");
                return;
            }

            //====================================
            Logger.Info("Preapre to create config file backup");
            CreateConfigBackup();

            //====================================
            ReplaceVersions();

            //====================================
            ReplaceGameVersions();

            //====================================
            Logger.Info(new string('-', 48));
            Logger.Debug("Successfully Version Manager Processed");
        }

        private void CreateConfigBackup()
        {
            //=====================================
            var name = Path.GetFileNameWithoutExtension(DefaultEngineFilePath);
            var path = Path.GetDirectoryName(DefaultEngineFilePath) ?? ConfigDirectoryPath;

            //=====================================
            var backupName = $"{name}-{StoreVersion}.ini";
            var backupPath = Path.Combine(path, "Backups", backupName);

            //=====================================
            File.Copy(DefaultEngineFilePath, backupPath);

            //=====================================
            Logger.Info($"Created config file backup: {backupName}");
        }

        private void ReplaceGameVersions()
        {
            Logger.Warn("Prepare generate GooglePlayGameVersion.h");
            //=====================================
            var sb = new StringBuilder(1024);

            //=====================================
            var gameAdminComment = $"{Manifest.DeployTarget} | {Manifest.GameConfiguration.AdminComment} | {GetDeployMessage(Languages.Russian)}";
            var servAdminComment = $"{Manifest.DeployTarget} | StoreVersionId: {NextStoreVersion} | {Manifest.ServerConfiguration.AdminComment} | {GetDeployMessage(Languages.Russian)}";

            //=====================================
            NextGameVersionModel = new GameVersionModel
            {
                StoreVersionId = NextStoreVersion,
                DeployTarget = Manifest.DeployTarget,
                ServerVersionId = NextTargetServerVersion,
                DeprecatePreviewVersions = Manifest.GameConfiguration.DeprecatePreviewVersions,
                AdminComment = gameAdminComment.Substring(0, Math.Min(127, gameAdminComment.Length)),
            };

            //=====================================
            NextServerVersionModel = new ServerVersionModel
            {
                AdminComment = servAdminComment.Substring(0, Math.Min(127, servAdminComment.Length)),
                DeprecatePreviewVersions = Manifest.ServerConfiguration.DeprecatePreviewVersions,
                ServerVersionId = NextTargetServerVersion,
                DeployTarget = Manifest.DeployTarget
            };

            //=====================================
            if (Manifest.IsAllowUpdateGoogleVersionManifest == false)
            {
                Logger.Warn($"Skip generate GooglePlayGameVersion.h | IsAllowUpdateGoogleVersionManifest: {Manifest.IsAllowUpdateGoogleVersionManifest}");
                return;
            }

            //=====================================
            sb.AppendLine("#pragma once");
            sb.AppendLine("");
            sb.AppendLine("#ifndef H_GooglePlayGameVersion");
            sb.AppendLine("#define H_GooglePlayGameVersion");
            sb.AppendLine("");

            sb.AppendLine("//==============================");
            sb.AppendLine("#ifdef GAME_STORE_VERSION");
            sb.AppendLine("#undef GAME_STORE_VERSION");
            sb.AppendLine("#endif");
            sb.AppendLine("");

            sb.AppendLine("//==============================");
            sb.AppendLine("#ifdef GAME_TARGET_SERVER");
            sb.AppendLine("#undef GAME_TARGET_SERVER");
            sb.AppendLine("#endif");
            sb.AppendLine("");

            sb.AppendLine("//==============================");
            sb.AppendLine("#ifdef GAME_TARGET_DEPLOY");
            sb.AppendLine("#undef GAME_TARGET_DEPLOY");
            sb.AppendLine("#endif");
            sb.AppendLine("");

            sb.AppendLine("//==============================");
            sb.AppendLine($"#define GAME_STORE_VERSION {NextGameVersionModel.StoreVersionId}");
            sb.AppendLine("");

            sb.AppendLine($"#define GAME_TARGET_SERVER {NextGameVersionModel.ServerVersionId}");
            sb.AppendLine("");

            sb.AppendLine($"#define GAME_TARGET_DEPLOY {(int)NextGameVersionModel.DeployTarget}");
            sb.AppendLine("");

            sb.AppendLine("#endif");

            //=====================================
            File.WriteAllText(GooglePlayGameVersionFilePath, sb.ToString());

            //=====================================
            Logger.Debug("Successfully generated GooglePlayGameVersion.h");
        }

        private void ReplaceVersions()
        {
            var sb = new StringBuilder(512);

            //=====================================
            ConfigFileContent = ConfigFileContent.Replace($"StoreVersion={StoreVersion}", $"StoreVersion={NextStoreVersion}");
            ConfigFileContent = ConfigFileContent.Replace($"VersionDisplayName={VersionDisplayName}", $"VersionDisplayName={NextVersionDisplayName}");
            ConfigFileContent = ConfigFileContent.Replace($"GameDeployTarget={(int)GameDeployTarget}", $"GameDeployTarget={(int)NextGameDeployTarget}");

            if (Manifest.IsAllowServerVersionIncrement)
            { 
                ConfigFileContent = ConfigFileContent.Replace($"TargetServerVersion={TargetServerVersion}", $"TargetServerVersion={NextTargetServerVersion}");
            }

            //=====================================
            File.WriteAllText(DefaultEngineFilePath, ConfigFileContent);

            //=====================================
            sb.AppendLine("Config file successfully modified");
            sb.AppendLine($"StoreVersion: {StoreVersion} -> {NextStoreVersion}");
            sb.AppendLine($"VersionDisplayName : {VersionDisplayName} -> {NextVersionDisplayName}");
            sb.AppendLine($"GameDeployTarget : [{GameDeployTarget} / {(int)GameDeployTarget}] -> [{NextGameDeployTarget} / {(int)NextGameDeployTarget}]");
           
            sb.AppendLine($"TargetServerVersion: {TargetServerVersion} -> {NextTargetServerVersion} | IsAllowServerVersionIncrement: {Manifest.IsAllowServerVersionIncrement}");

            //=====================================
            Logger.Info(sb.ToString());
        }

        private void LoadConfigFileContent()
        {
            ConfigFileContent = File.ReadAllText(DefaultEngineFilePath);
        }

        private string ParseConfigValue(Regex regex, string name)
        {
            try
            {
                Logger.Info(new string('-', 48));
                Logger.Info($"Preapre to parse {name}");

                //====================================
                var match = regex.Match(ConfigFileContent);

                //====================================
                if (match.Success)
                {
                    //-----------------------------------
                    Logger.Info($"Successfully parse {name} | {match.Value} | Groups: {match.Groups.Count}");

                    //-----------------------------------
                    var option = match.Groups[1].Value;

                    //-----------------------------------
                    Logger.Warn($"Successfully detected {name} : {option}");

                    //-----------------------------------
                    return option;
                }
                else
                {
                    throw new Exception($"Failed parse {name} | Regex.Match.Success = false");
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"Failed parse {name}", e);
                throw e;
            }
        }

        private static Regex RegexTargetServerVersion { get; } = new Regex("TargetServerVersion=([0-9]*)", RegexOptions.Compiled | RegexOptions.Singleline);
        private void ParseTargetServerVersion()
        {
            TargetServerVersion = int.Parse(ParseConfigValue(RegexTargetServerVersion, "TargetServerVersion"));
        }

        private static Regex RegexStoreVersion { get; } = new Regex("StoreVersion=([0-9]*)", RegexOptions.Compiled | RegexOptions.Singleline);
        private void ParseStoreVersion()
        {
            StoreVersion = int.Parse(ParseConfigValue(RegexStoreVersion, "Store Version"));
        }

        private static Regex RegexVersionDisplayName { get; } = new Regex("VersionDisplayName=([0-9*]{1,}.[0-9*]{1,}.[0-9*]{1,})", RegexOptions.Compiled | RegexOptions.Singleline);
        private void ParseVersionDisplayName()
        {
            VersionDisplayName = ParseConfigValue(RegexVersionDisplayName, "Display Version");
        }


        private static Regex RegexGameDeployTarget { get; } = new Regex("GameDeployTarget=([0-9])", RegexOptions.Compiled | RegexOptions.Singleline);
        private void ParseGameDeployTarget()
        {
            var str = ParseConfigValue(RegexGameDeployTarget, "Game Deploy Target");
            var tmp = DeployTarget.None;
            var res = Enum.TryParse<DeployTarget>(str, true, out tmp);
            if (res)
            {
                Logger.Debug($"[ParseGameDeployTarget][successfully][{tmp} / {(int)tmp}]");
            }
            else
            {
                Logger.Error($"[ParseGameDeployTarget][failed][{str}]");
            }
            GameDeployTarget = tmp;
        }

    }
}
