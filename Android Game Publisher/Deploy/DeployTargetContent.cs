﻿namespace AndroidGamePublisher.Deploy
{
    public enum DeployTargetContent
    {
        None,
        SkipCook,
        Cook
    }
}
