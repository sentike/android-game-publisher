﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndroidGamePublisher.Deploy
{
    public enum BuildConfiguration
    {
        Shipping,
        Development,
        Debug
    }

    public enum ApplicationType
    {
        Game,
        Server,
    }

    public enum ApplicationPlatform
    {
        Android,
        Win32,
        Win64,
    }

    public enum ApplicationPlatformBitDepth
    {
        All,
        X86,
        X64,
    }


    public class DeployConfiguration
    {
        public bool DeployProject { get; set; }

        public bool IsAllowAutoDeploy { get; set; }
        public DeployTargetContent DeployTargetContentOverride { get; set; }
        public string DeployTargetContentComment { get; set; } = "None | SkipCook | Cook";


        public string ApplicationTypeComment { get; set; } = "Game | Server";
        public ApplicationType ApplicationType { get; set; } = ApplicationType.Server;

        public string ClientConfigComment { get; set; } = "Shipping | Development | Debug";
        public BuildConfiguration ClientConfig { get; set; } = BuildConfiguration.Development;

        public string ServerConfigComment { get; set; } = "Shipping | Development | Debug";
        public BuildConfiguration ServerConfig { get; set; } = BuildConfiguration.Development;

        public string PlatformComment { get; set; } = "Android | Win32 | Win64";
        public ApplicationPlatform Platform { get; set; }

        public string PlatformBitDepthComment { get; set; } = "All | X86 | X64";
        public ApplicationPlatformBitDepth PlatformBitDepth { get; set; }

        public string OutputPath { get; set; }
        public string ProjectName { get; set; }
        public string BinariesName { get; set; }
        public string BoundleName { get; set; }

        public bool CrashReporter { get; set; } = true;
        public bool NoP4 { get; set; } = true;
        public bool Distribution { get; set; } = true;

        
        public bool NoDebuginfo { get; set; } = true;
        public bool Package { get; set; } = true;
        public bool UsePak { get; set; } = true;

        public string AdditionalArguments { get; set; }
        public bool DeprecatePreviewVersions { get; set; }
        public string AdminComment { get; set; }
    }
}
