﻿namespace AndroidGamePublisher.Deploy
{
    public enum DeployTarget
    {
        None,

        Internal,
        Alpha,
        Beta,

        Production,

    }
}
