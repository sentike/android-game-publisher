﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AndroidGamePublisher.Deploy.Project;
using AndroidGamePublisher.Extensions;
using AndroidGamePublisher.Version;
using CliWrap;
using CliWrap.Models;

namespace AndroidGamePublisher.Deploy.Server
{
    public class ServerDeployManager : ProjectDeployManager
    {
        public SharpSvn.SvnClient SvnClient { get; } = new SharpSvn.SvnClient();


        public override string GetPlatformBitDepth(ApplicationPlatformBitDepth platformBitDepth)
        {
            Logger.Error($"ServerDeployManager.GetPlatformBitDepth not implemented");
            return platformBitDepth.ToString();
        }

        protected override bool PreDeployValidation()
        {
            return base.PreDeployValidation();
        }

        private void SvnCleanup(string InWorkingDirectory)
        {
            Logger.Warn(new string('=', 48));
            try
            {
                Logger.Warn($"- Begin svn cleanup in {InWorkingDirectory}");
                var ExecuteStatus =  SvnClient.CleanUp(InWorkingDirectory, new SharpSvn.SvnCleanUpArgs { BreakLocks = true, ClearDavCache = true, FixTimestamps = true, VacuumPristines = true });
                Logger.Debug($"- Svn cleanup success: {ExecuteStatus}");
            }
            catch (Exception e)
            {
                Logger.Error($"- Failed svn cleanup in {InWorkingDirectory} | Exception: {e.Message}");
                Logger.WriteExceptionMessage($"- Failed svn cleanup  in {InWorkingDirectory}", e);
            }
         }

        public string TrunkRepositoryUrl => $"{DeployManifest?.GameServerRepositoryUrl}/trunk";
        public string NextBranchRepositoryUrl => $"{DeployManifest?.GameServerRepositoryUrl}/branches/Server_{VersionManager?.NextTargetServerVersion}";


        private void SvnCreateBranch(string InWorkingDirectory)
        {
            try
            {
                //-----------------------------------------
                Logger.Warn(new string('=', 48));
                Logger.Warn($"- Begin svn create {VersionManager.NextTargetServerVersion} branch ..");

                //-----------------------------------------
                var Message = $"Creating branch of server version {VersionManager.TargetServerVersion} | StoreVersion: {VersionManager.StoreVersion} -> {VersionManager.NextStoreVersion}";

                //-----------------------------------------
                Logger.Info($"TrunkUrl: {TrunkRepositoryUrl}");
                Logger.Info($"BranchUrl: {NextBranchRepositoryUrl}");
                Logger.Info($"Message: {Message}");
                Logger.Warn(new string('=', 48));

                //=================================
                var TrunkRepositoryUri = new Uri(TrunkRepositoryUrl);
                var TrunkRepositorySvn = SharpSvn.SvnTarget.FromUri(TrunkRepositoryUri);

                //=================================
                var NextBranchRepositoryUri = new Uri(NextBranchRepositoryUrl);

                //-----------------------------------------
                var ExecuteStatus = SvnClient.RemoteCopy(TrunkRepositoryUri, NextBranchRepositoryUri, new SharpSvn.SvnCopyArgs { CreateParents = true, LogMessage = Message }, out var SvnCommitResult);
                
                //-----------------------------------------
                //  Проверяем есть ли сообщение о успешном создании ветки
                if (ExecuteStatus)
                {
                    Logger.Debug($"! Creating branch of server version was successfully, Revision: {SvnCommitResult?.Revision} | RepositoryRoot: {SvnCommitResult?.RepositoryRoot} | PostCommitError: {SvnCommitResult?.PostCommitError} {Environment.NewLine}");
                }
                else
                {
                    Logger.Error($"! Creating branch of server version was failed, Revision: {SvnCommitResult?.Revision} | RepositoryRoot: {SvnCommitResult?.RepositoryRoot} | PostCommitError: {SvnCommitResult?.PostCommitError} {Environment.NewLine}");
                }

                //-----------------------------------------
                Logger.Warn(new string('=', 48));
                Logger.Debug($"> svn copy {TrunkRepositoryUrl} {NextBranchRepositoryUrl} -m=\"{Message}\"");

                Logger.Warn(new string('=', 48));
                SvnCommit(InWorkingDirectory, Message);

                //-----------------------------------------
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed svn create branch", e);
                throw;
            }        
        }

        private void SvnSwitchBranch(string InWorkingDirectory, string InBranchUrl)
        {
            try
            {
                Logger.Warn(new string('=', 48));
                Logger.Info("- Begin svn switch branch to ..");
                Logger.Info(InBranchUrl);

                var ExecuteStatus = SvnClient.Switch(InWorkingDirectory, new SharpSvn.SvnUriTarget(InBranchUrl), new SharpSvn.SvnSwitchArgs(), out var ExecuteSwitchResult);
            
                if (ExecuteStatus && (ExecuteSwitchResult?.Revision ?? 0) > 0)
                {
                    Logger.Debug($"- svn switch branch successfully, Revision: {ExecuteSwitchResult?.Revision}");
                }
                else
                {
                    Logger.Error($"- svn switch branch failed, Revision: {ExecuteSwitchResult?.Revision}");
                }
            }
            catch (Exception e)
            {
                Logger.Error($"- Failed svn switch from {InWorkingDirectory} to {InBranchUrl} | {Environment.NewLine} Exception: {e.Message}");
                Logger.WriteExceptionMessage($"- Failed svn switch from {InWorkingDirectory} to {InBranchUrl} | {Environment.NewLine}", e);
                throw;
            }

            //-------------------------------
            //  Отчищаем блокировки после смены ветки
            SvnCleanup(InWorkingDirectory);

            //-------------------------------
            SvnCommit(InWorkingDirectory, $"Commit after switch to {InBranchUrl}");
        }

        private void SvnMerge(string InWorkingDirectory, string InFromBranchUrl)
        {
            try
            {
                SvnClient.GetInfo(SharpSvn.SvnTarget.FromString(InWorkingDirectory), out var SvnInfo);

                Logger.Warn(new string('=', 48));
                Logger.Info("- Begin svn merge from ..");
                Logger.Info($"InFromBranchUrl: {InFromBranchUrl}");
                Logger.Info($"Revision: {SvnInfo.Revision}");

                var MergeRange = SharpSvn.SvnRevisionRange.FromRevision(SvnInfo.Revision);
                var ExecuteStatus = SvnClient.Merge
                (
                    InWorkingDirectory, 
                    InFromBranchUrl,
                    MergeRange, 
                    new SharpSvn.SvnMergeArgs { Force = true}
                );

                if (ExecuteStatus)
                {
                    Logger.Debug("- svn merge successfully");
                }
                else
                {
                    Logger.Warn("- svn merge failed");
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed svn merge", e);
                throw;
            }
        }

        private void SvnFixConflicts(string InWorkingDirectory)
        {
            Logger.Warn(new string('=', 48));

            var ConflictedFiles = GetConflictedFiles(InWorkingDirectory);
            Logger.Info($"- Begin svn fix {ConflictedFiles.Length} conflicts ..");

            foreach (var ConflictedFile in ConflictedFiles)
            {
                var status = SvnClient.Resolve(ConflictedFile, SharpSvn.SvnAccept.TheirsFull);
                Logger.Warn($"> {ConflictedFile} | Fixed: {status}");
            }
        }

        private void SvnCommit(string InWorkingDirectory, string InCommitMessage)
        {
            try
            {
                Logger.Warn(new string('=', 48));
                Logger.Info("- Begin svn commit ..");
                Logger.Info($"Message: {InCommitMessage}{Environment.NewLine}");

                var ExecuteStatus = SvnClient.Commit(InWorkingDirectory, new SharpSvn.SvnCommitArgs { LogMessage = InCommitMessage }, out var SvnCommitResult);
                if(ExecuteStatus)
                {
                    Logger.Debug($"- svn commit successfully, Revision: {SvnCommitResult?.Revision} | RepositoryRoot: {SvnCommitResult?.RepositoryRoot} {Environment.NewLine}");
                }
                else
                {
                    Logger.Debug($"- svn commit failed, Error: {SvnCommitResult?.PostCommitError} {Environment.NewLine}");
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed svn commit. InWorkingDirectory: {InWorkingDirectory}", e);
                throw;
            }
        }

        private string[] GetConflictedFiles(string WorkingDirectory)
        {
            SvnClient.GetStatus(WorkingDirectory, out var SvnStatus);
            return SvnStatus.Where(p => p.LocalContentStatus == SharpSvn.SvnStatus.Conflicted || p.LocalNodeStatus == SharpSvn.SvnStatus.Conflicted).Select(p => p.Path).ToArray();
        }

        private string[] GetDeletedFiles(string WorkingDirectory)
        {
            SvnClient.GetStatus(WorkingDirectory, out var SvnStatus);
            return SvnStatus.Where(p => p.LocalContentStatus == SharpSvn.SvnStatus.Missing || p.LocalNodeStatus == SharpSvn.SvnStatus.Missing).Select(p => p.Path).ToArray();
        }

        protected override void PostDeployProcess()
        {
            var WorkingDirectory = Path.Combine(Configuration.OutputPath, "WindowsServer");
            //======================================
            //  Коммитим изменения в Trunk, что бы в основной ветке были последние изменения для уменьшения размера дочерних веток
            try
            {
                WatchDog.Start();

                //======================================
                SvnCleanup(WorkingDirectory);

                //======================================
                SvnClient.Add(Path.Combine(Configuration.OutputPath, "WindowsServer", "Engine"), new SharpSvn.SvnAddArgs { AddParents = true, Force = true });
                SvnCommit(WorkingDirectory, $"Engine directory updated, TargetServerVersion: {VersionManager.NextTargetServerVersion}");

                //======================================
                SvnClient.Add(Path.Combine(Configuration.OutputPath, "WindowsServer", Configuration.ProjectName), new SharpSvn.SvnAddArgs { AddParents = true, Force = true });
                SvnCommit(WorkingDirectory, $"Game directory updated, TargetServerVersion: {VersionManager.NextTargetServerVersion}");


                //======================================
                var DeletedFiles = GetDeletedFiles(WorkingDirectory);
                foreach (var file in DeletedFiles)
                {
                    SvnClient.Delete(file);
                }

                //======================================
                Logger.Info("- Begin svn commit");
                SvnCommit(WorkingDirectory, $"Server build for game version: {VersionManager.StoreVersion} -> {VersionManager.NextStoreVersion} {Environment.NewLine}. Commit at {DateTime.UtcNow} {Environment.NewLine} Update Message: {Environment.NewLine} {VersionManager.GetDeployMessage(Languages.Russian)}");

                //======================================
                WatchDog.Stop();

                //======================================
                Logger.Warn($"- Svn final commited successfully");
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed svn final commit", e);
                throw;
            }

            //===================================================
            //  Если весия сервера обновилась
            if (VersionManager.TargetServerVersion != VersionManager.NextTargetServerVersion)
            {
                //----------------------------------------------
                //  Создаем новую ветку для текущего обновления
                SvnCreateBranch(WorkingDirectory);

                //----------------------------------------------
                //  Сменяет текущую ветку на новую
                SvnSwitchBranch(WorkingDirectory, NextBranchRepositoryUrl);
            }
            else
            {
                //----------------------------------------------
                //  Сменяет текущую ветку на новую (trunk -> server version)
                SvnSwitchBranch(WorkingDirectory, NextBranchRepositoryUrl);

                //----------------------------------------------
                SvnCleanup(WorkingDirectory);

                //----------------------------------------------
                //  Получаем последние обновления из Trunk ветки
                SvnMerge(WorkingDirectory, TrunkRepositoryUrl);

                SvnFixConflicts(WorkingDirectory);

                //----------------------------------------------
                SvnCommit(WorkingDirectory, $"Merge from trunk complete | TargetServerVersion: {VersionManager.TargetServerVersion} | StoreVersion: {VersionManager.StoreVersion} -> {VersionManager.NextStoreVersion}");
            }

            //=================================
            try
            {
                if (DeployManifest.IsAllowGitCreateTagVersion)
                {
                    if (VersionManager.TargetServerVersion != VersionManager.NextTargetServerVersion)
                    {
                        using (var cli = new Cli("git", new CliSettings
                        {
                            WorkingDirectory = GameRootPath
                        }))
                        {
                            cli.Execute($"tag -a \"ServerV{VersionManager.NextTargetServerVersion}\" -m \"TargetServerVersion {VersionManager.TargetServerVersion} -> {VersionManager.NextTargetServerVersion}\"");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed git version tag", e, false);
            }

            //=================================
            PostDeployMasterServerNotify(VersionManager.NextServerVersionModel, DeployManifest.MasterServerPostDeployServerUrl);
        }

 

        protected override void PreDeployProcess()
        {
            base.PreDeployProcess();
        }

        protected override void DeployProcess()
        {
            base.DeployProcess();
        }

        protected override void PostDeployValidation()
        {
            //==============================================================
            var bIsExistsProjectDirectory = IsExistsProjectDirectory();
            var bIsExistsEngineDirectory = IsExistsEngineDirectory();
            var bIsExistsGameDirectory = IsExistsGameDirectory();
            var bIsExistsGameBinaries = IsExistsGameBinaries();
            var bIsExistsGameContent = IsExistsGameContent();

            //==============================================================        
            var isValid = bIsExistsProjectDirectory && bIsExistsEngineDirectory && bIsExistsGameDirectory && bIsExistsGameBinaries && bIsExistsGameContent;
            if (isValid)
            {
                Logger.Warn("- PostDeployValidation: Valid");
            }
            else
            {
                Logger.Info("- PostDeployValidation: INVALID");
                throw new ArgumentException("Failed final content validation");
            }
        }


        protected bool IsExistsEngineDirectory()
        {
            var path = Path.Combine(Configuration.OutputPath, "WindowsServer", "Engine");
            var bIsExistsEngineDirectory = Directory.Exists(path);
            Logger.Info($"- IsExistsEngineDirectory: {bIsExistsEngineDirectory} | path: {path}");
            return bIsExistsEngineDirectory;
        }

        protected bool IsExistsGameDirectory()
        {
            var path = Path.Combine(Configuration.OutputPath, "WindowsServer", Configuration.ProjectName);
            var bIsExistsGameDirectory = Directory.Exists(path);
            Logger.Info($"- IsExistsGameDirectory: {bIsExistsGameDirectory} | path: {path}");
            return bIsExistsGameDirectory;
        }

        protected override bool IsExistsGameContent()
        {
            var path = Path.Combine(Path.Combine(Configuration.OutputPath, "WindowsServer", Configuration.ProjectName, "Content"));
            var bIsExistsGameContent = Directory.Exists(path);
            Logger.Info($"- IsExistsGameContent: {bIsExistsGameContent} | path: {path}");
            return bIsExistsGameContent;
        }


        protected override bool IsExistsGameBinaries()
        {
            var FileName = $"{Configuration.BinariesName}{Configuration.ApplicationType}{GetServerConfig(Configuration.ServerConfig)}.exe";
            var path = Path.Combine(Configuration.OutputPath, "WindowsServer", Configuration.ProjectName, "Binaries", "Win64", FileName);
            var bIsExistsGameBinaries = File.Exists(path);
            Logger.Info($"- IsExistsGameBinaries: {bIsExistsGameBinaries} | path: {path}");
            return bIsExistsGameBinaries;
        }

        public override DeployConfiguration Configuration => DeployManifest?.ServerConfiguration;

        protected override void ProcessDeleteLatestDeploy()
        {
            Logger.Error(new string('=', 48));
            Logger.Error("ProcessDeleteLatestDeploy");

            //===================================================
            var WindowsServerPath = Path.Combine(Configuration.OutputPath, "WindowsServer");

            //===================================================
            //  Отчищаем блокировки
            SvnCleanup(WindowsServerPath);

            //  Сменяет текущую ветку на основую
            SvnSwitchBranch(WindowsServerPath, TrunkRepositoryUrl);

            //===================================================
            //  Обновляем ветку до последних изменений
            //  Обновляем сам репозиторий (.svn)
            try
            {
                Logger.Info($"- Prepare to svn server update #1");
                Logger.Warn($"WindowsServerPath: {WindowsServerPath}");
                SvnClient.Update(WindowsServerPath, new SharpSvn.SvnUpdateArgs { UpdateParents = true });
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed svn update for server before delete latest deploy", e, false);
            }
   
            Logger.Error(new string('=', 48));
            //===================================================
            var engine = Path.Combine(WindowsServerPath, "Engine");
            var project = Path.Combine(WindowsServerPath, Configuration.ProjectName);

            //===================================================
            Logger.Info($"- Prepare to delete {engine}");

            //===================================================
            try
            {
                if (Directory.Exists(engine))
                {
                    Directory.Delete(engine, true);
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed delete WindowsServer/Engine. Path: {engine}", e, false);
            }

            //===================================================
            Logger.Info($"- Prepare to project {project}");

            //===================================================
            try
            {
                if (Directory.Exists(project))
                {
                    Directory.Delete(project, true);
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed delete WindowsServer/Project. Path: {engine}", e, false);
            }
        }


        public ServerDeployManager(DeployManifest manifest, VersionManager versionManager) : base(manifest, versionManager)
        {
        }
    }
}
