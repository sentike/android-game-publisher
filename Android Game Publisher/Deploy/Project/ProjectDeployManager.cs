﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AndroidGamePublisher.Extensions;
using AndroidGamePublisher.Version;
using CliWrap;
using CliWrap.Models;
using CliWrap.Services;
using NLog;
using NLog.Common;
using RestSharp;

namespace AndroidGamePublisher.Deploy.Project
{
    public abstract class ProjectDeployManager
    {
        public static string GameProjectPath { get; set; }
        public static string UnrealEnginePath { get; set; }
        public static string GameRootPath => Path.GetDirectoryName(GameProjectPath);
        public static string GameContentPath => Path.Combine(GameRootPath, "Content");


        // ReSharper disable once InconsistentNaming
        public static string RunUATPath => Path.Combine(UnrealEnginePath, "Engine", "Build", "BatchFiles", "RunUAT.bat");
        public static string UnrealEngineLogsPath => Path.Combine(UnrealEnginePath, "Engine", "Programs", "AutomationTool", "Saved", "Logs");

        // ReSharper disable once InconsistentNaming
        protected void RunUAT(string[] inArguments = null)
        {
            var sb = new StringBuilder(2048);

            //============================================
            Logger.Info($"- Starting UAT ...");
            Logger.Info($"* RunUATPath: {RunUATPath}");
            Logger.Info($"* ProjectPath: {GameProjectPath}");
            Logger.Info($"* OutputPath: {Configuration.OutputPath}");
            Logger.Info($"* ApplicationType: {Configuration.ApplicationType}");
            Logger.Info($"* ClientConfig: {Configuration.ClientConfig}");
            Logger.Info($"* ServerConfig: {Configuration.ServerConfig}");
            Logger.Info($"* Platform: {Configuration.Platform} |  {Configuration.PlatformBitDepth}");
            Logger.Info($"* CrashReporter: {Configuration.CrashReporter}");

            //============================================
            //sb.Append(RunUATPath);
            //sb.Append(" ");

            //============================================
            sb.Add("BuildCookRun", String.Empty);
            sb.AddEscaped("project", GameProjectPath);

            //============================================
            if (Configuration.CrashReporter)
            {
                sb.Add("CrashReporter");
            }

            //============================================
            if (Configuration.NoP4)
            {
                sb.Add("NoP4");
            }

            //============================================
            if (DeployManifest.DeployTargetContent == DeployTargetContent.SkipCook)
            {
                if (Configuration.DeployTargetContentOverride != DeployTargetContent.Cook)
                {
                    Logger.Error($"{Configuration.ApplicationType} content cook skiped | Global");
                    sb.Add("skipcook");
                }
            }
            else if (Configuration.DeployTargetContentOverride != DeployTargetContent.Cook)
            {
                Logger.Error($"{Configuration.ApplicationType} content cook skiped | Local");
                sb.Add("skipcook");
            }


            //============================================
            sb.Add("cook");
            sb.Add("build");
            sb.Add("stage");
            sb.Add("archive");
            sb.Add("utf8output");
            sb.Add("compileereqs");
            sb.Add("prereqs");

            //============================================
            sb.AddPair("clientconfig", Configuration.ClientConfig);
            sb.AddPair("serverconfig", Configuration.ServerConfig);

            sb.AddPair("targetplatform", Configuration.Platform);
            sb.AddPair("platform", Configuration.Platform);

            //============================================
            if (Configuration.ApplicationType == ApplicationType.Game)
            {
                if (Configuration.Distribution)
                {
                    sb.Add("distribution");
                }

                if (Configuration.NoDebuginfo)
                {
                    sb.Add("NoDebuginfo");
                }

                if (Configuration.Package)
                {
                    sb.Add("Package");
                }

                if (Configuration.UsePak)
                {
                    sb.Add("pak");
                }
            }
            else
            {
                sb.Add("server");
                sb.Add("noclient");
            }

            //============================================
            sb.AddEscaped("archivedirectory", Configuration.OutputPath);

            //============================================
            sb.Append(Configuration.AdditionalArguments);

            //============================================
            var arguments = new[]
            {
                sb.ToString(),
                string.Join(" ", inArguments ?? new string[0])
            };

            //============================================
            var argument = string.Join(" ", arguments);

            //============================================
            Logger.Warn($"UAT path: {RunUATPath}");
            Logger.Warn($"UAT command line: {argument}");

            StartAndWaitProcess(RunUATPath, argument, out int exitCode);

            //============================================
            Logger.Info($"- Complete UAT execute | Exit Code: {exitCode}");

            //============================================
            if(exitCode != 0)
            {
                StartProcess(UnrealEngineLogsPath, String.Empty);
                throw new ApplicationException($"- Complete UAT execute with error | Exit Code: {exitCode}");
            }
        }



        protected ProjectDeployManager(DeployManifest manifest, VersionManager versionManager)
        {
            DeployManifest = manifest;
            VersionManager = versionManager;
        }

        public DeployManifest DeployManifest { get; }
        public abstract DeployConfiguration Configuration { get; } 

        public VersionManager VersionManager { get; }
        public TimeWatchDog WatchDog { get; } = new TimeWatchDog();

        public static void StartProcess(string path, string arguments)
        {
            //============================================
            using (var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = path,
                    Arguments = arguments ?? string.Empty
                }
            })
            {
                process.Start();
            }
        }

        public abstract string GetPlatformBitDepth(ApplicationPlatformBitDepth platformBitDepth);


        public string GetClientConfig(BuildConfiguration configuration)
        {
            if (configuration == BuildConfiguration.Shipping)
            {
                return $"-{Configuration.Platform}-Shipping";
            }
            else if (configuration == BuildConfiguration.Debug)
            {
                return $"-{Configuration.Platform}-DebugGame";
            }
            return string.Empty;
        }

        public string GetServerConfig(BuildConfiguration configuration)
        {
            if (configuration == BuildConfiguration.Shipping)
            {
                return $"-{Configuration.Platform}-Shipping";
            }
            else if (configuration == BuildConfiguration.Debug)
            {
                return $"-{Configuration.Platform}-Debug";
            }
            return string.Empty;
        }


        protected void StartAndWaitProcess(string path, string arguments, out int code)
        {
            //============================================
            code = -1;

            //============================================
            using (var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = path,
                    Arguments = arguments ?? string.Empty
                }
            })
            {
                //============================================
                if (process.Start())
                {
                    //------------------
                    //  Ждем когда обновится информация о процессе
                    Thread.Sleep(1000);

                    //------------------
                    //  Ждем завершения процесса
                    process.WaitForExit();

                    //------------------
                    code = process.ExitCode;
                }
            }
        }

        protected abstract void ProcessDeleteLatestDeploy();

        protected void DeleteLatestDeploy()
        {
            //============================================
            Logger.Info("- Begin delete last build");

            //============================================
            ProcessDeleteLatestDeploy();

            //============================================
            Logger.Info("- Last build successfully deleted");
        }

        protected void UpdateSourceCode()
        {                      
            //======================================
            ExecutionOutput execute = null;

            //============================================
            Logger.Info(new string('=', 48));
            Logger.Warn("- Begin git update ...");
            Logger.Info("- Game path: " + GameRootPath);
            Logger.Info(new string('=', 48));

            if (DeployManifest.SkipGitCheckUpdates)
            {
                Logger.Warn("! Check source code updates was disabled \n");
                return;
            }

            //============================================
            try
            {
                using (var cli = new Cli("git", new CliSettings
                {
                    WorkingDirectory = GameRootPath
                }))
                {
                    //=====================================
                    WatchDog.Start();

                    //=====================================
                    var handler = new BufferHandler
                    (
                        stdOutLine => Logger.Info($"> {stdOutLine}"),
                        stdErrLine => Logger.Error($"! {stdErrLine}")
                    );

                    //======================================
                    cli.Execute("--version", bufferHandler: handler).ThrowIfError();
                    cli.Execute("fetch origin", bufferHandler: handler);
                    execute = cli.Execute("pull origin master", bufferHandler: handler);

                    //======================================
                    WatchDog.Stop();

                    //======================================
                    Logger.Warn($"- Git update source code successfully. Exit Code: {execute.ExitCode} | Elapsed: {WatchDog.Elapsed?.TotalSeconds} seconds");
                }
            }
            catch (Exception e)
            {
                Logger.Error($"- Failed git update. Exit code: {execute?.ExitCode} | Exception: {e.Message}");
                Logger.WriteExceptionMessage($"- Failed git update. Exit code: {execute?.ExitCode}", e);
            }

            Logger.Info(new string('=', 48));
        }

        protected void UpdateGameContent()
        {
            //============================================
            int exitCode = -2;

            //============================================
            var content = Path.Combine(Path.GetDirectoryName(GameProjectPath) ?? throw new ArgumentNullException(nameof(GameProjectPath), "Failed get directory name for Content"), "Content");

            //============================================
            Logger.Info(new string('=', 48));
            Logger.Warn("- Begin svn update ...");
            Logger.Info("- Content path: " + content);
            Logger.Info(new string('=', 48));

            //============================================
            if (DeployManifest.SkipSvnCheckUpdates)
            {
                Logger.Warn("! Check game content updates was disabled \n");
                return;
            }
         
            //============================================
            try
            {
                //============================================
                WatchDog.Start();
                StartAndWaitProcess("svn", $"update \"{content}\"", out exitCode);
                WatchDog.Stop();
                
                //============================================
                Logger.Info($"- Complete svn update, elapsed {WatchDog.Elapsed?.TotalSeconds} seconds. Exit code: {exitCode}");
            }
            catch (Exception e)
            {
                Logger.Error($"- Failed svn update. Exit code: {exitCode} | Exception: {e.Message}");
                Logger.WriteExceptionMessage($"- Failed svn update. Exit code: {exitCode}", e);
                throw;
            }

            Logger.Info(new string('=', 48));
        }

        protected virtual bool IsExistsProjectDirectory()
        {
            var bIsExistsProjectDirectory = Directory.Exists(Configuration.OutputPath);
            Logger.Info($"- IsExistsProjectDirectory: {bIsExistsProjectDirectory} | Path: {Configuration.OutputPath}");
            return bIsExistsProjectDirectory;
        }

        protected abstract bool IsExistsGameContent();
        protected abstract bool IsExistsGameBinaries();

        protected static ILogger Logger { get; } = LogManager.GetCurrentClassLogger();

        public virtual bool Process()
        {
            //========================================================
            try
            {
                return ProcessProxy();
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"Exception on deployment process for {Configuration.ApplicationType} - {Configuration.Platform} - {Configuration.PlatformBitDepth}", e);
                return false;
            }

            //========================================================
        }

        protected virtual bool ProcessProxy()
        {
            //========================================================
            Logger.Info(new string('=', 48));
            Logger.Warn($"Starting deployment process for {Configuration.ApplicationType} - {Configuration.Platform} - {Configuration.PlatformBitDepth} ");

            //========================================================
            if (Configuration.DeployProject == false)
            {
                Logger.Debug("The project deployment was skipped.");
                return true;
            }

            //========================================================
            Logger.Warn("Starting pre deploy validation ...");
            if (PreDeployValidation() == false)
            {
                Logger.Error("- Failed process pre deploy validation");
                return false;
            }

            //========================================================
            Logger.Warn("Starting pre deploy process ...");
            PreDeployProcess();

            //========================================================
            Logger.Warn("Starting main deploy process ...");
            DeployProcess();

            //========================================================
            Logger.Warn("Starting post deploy process ...");
            PostDeployProcess();

            //========================================================
            Logger.Warn("Starting post deploy validation ...");
            PostDeployValidation();

            //========================================================
            Logger.Debug($"Successfully completed the {Configuration.ApplicationType} - {Configuration.Platform} - {Configuration.PlatformBitDepth} build!");
            Logger.Debug($"OutputPath: {Configuration.OutputPath}");

            //========================================================
            Logger.Warn("For detailed log, view");
            Logger.Warn(UnrealEngineLogsPath);
            return true;
        }

        protected virtual void DeployProcess()
        {
            RunUAT();
        }

        protected abstract void PostDeployProcess();


        protected virtual void PreDeployProcess()
        {
            if(Configuration.DeprecatePreviewVersions)
            {
                Logger.Error("! DeprecatePreviewVersions has enabled");
            }
            else
            {
                Logger.Warn("! DeprecatePreviewVersions has disabled");
            }

            //========================================================
            DeleteLatestDeploy();

            //========================================================
            UpdateGameContent();
            UpdateSourceCode();
        }

        protected virtual bool PreDeployValidation()
        {
            //========================================================
            if (string.IsNullOrWhiteSpace(GameProjectPath) || GameProjectPath.Contains("{") || File.Exists(GameProjectPath) == false)
            {
                Logger.Warn($"ProjectPath not exist | {GameProjectPath}");
                return false;
            }

            //========================================================
            if (string.IsNullOrWhiteSpace(RunUATPath) || RunUATPath.Contains("{") || File.Exists(RunUATPath) == false)
            {
                Logger.Warn($"RunUATPath not exist | {RunUATPath}");
                return false;
            }

            //========================================================
            if (string.IsNullOrWhiteSpace(Configuration.OutputPath) || Directory.Exists(Configuration.OutputPath) == false)
            {
                Logger.Warn($"OutputPath not exist | {Configuration.OutputPath}");
                return false;
            }

            //========================================================
            if (string.IsNullOrWhiteSpace(Configuration.BinariesName) || Configuration.BinariesName.Contains("{"))
            {
                Logger.Warn($"BinariesName was invalid: {Configuration.BinariesName}");
                return false;
            }

            //========================================================
            if (string.IsNullOrWhiteSpace(Configuration.ProjectName) || Configuration.ProjectName.Contains("{"))
            {
                Logger.Warn($"BinariesName was invalid: {Configuration.ProjectName}");
                return false;
            }

            return true;
        }

        protected void PostDeployMasterServerNotify(object Object, string url)
        {
            //=================================
            Logger.Info("Prepare to notify master server:");
            Logger.Info(url);

            //=================================
            var client = new RestClient(url);

            //=================================
            var request = new RestRequest(Method.POST)
            {
                RequestFormat = DataFormat.Json
            };

            //=================================
            request.AddJsonBody(Object);

            //=================================
            var response = client.Execute(request);

            //=================================
            if (response.IsSuccessful)
            {
                Logger.Debug("Successfully sent a notification to the master server.");
            }
            else
            {
                Logger.Warn($"============================================================");
                Logger.Error($"Failed sent a notification to the master server. | StatusCode: {response.StatusCode} | ErrorMessage: {response.ErrorMessage}");
                Logger.Error($"ErrorException: {response.ErrorException}{Environment.NewLine}");
                Logger.Warn($"============================================================{Environment.NewLine}");
                Logger.Error($"Content: {response.Content}{Environment.NewLine}");
                Logger.Warn($"============================================================");
            }
        }

        protected virtual void PostDeployValidation()
        {
            //==============================================================
            var bIsExistsProjectDirectory = IsExistsProjectDirectory();
            var bIsExistsGameBinaries = IsExistsGameBinaries();
            var bIsExistsGameContent = IsExistsGameContent();

            //==============================================================        
            var isValid = bIsExistsProjectDirectory && bIsExistsGameBinaries && bIsExistsGameContent;
            if (isValid)
            {
                Logger.Warn("- PostDeployValidation: Valid");
            }
            else
            {
                Logger.Info("- PostDeployValidation: INVALID");
                throw new ArgumentException("Failed final content validation");
            }
        }
    }
}
