﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AndroidGamePublisher.Deploy
{

    public enum DeployTargetTextureCompression
    {
        None,
        ETC1,
        ETC2,
        ASTC,
        PVRTC,
    }

    public class DeployManifest
    {
        public bool IsAllowUpdateGoogleVersionManifest { get; set; } = true;
        public int NumberOfGamePublishAttemps { get; set; } = 5;

        public string DisplayVersion { get; set; } = "0.6";

        public string TextureCompressionComment { get; set; } = "ETC1 | ETC2 | ASTC | PVRTC";
        public DeployTargetTextureCompression TextureCompression { get; set; } = DeployTargetTextureCompression.ETC2;

        public DeployTarget DeployTarget { get; set; }
        public string DeployTargetComment { get; set; } = "Internal | Alpha | Beta | Production";

        public DeployTargetContent DeployTargetContent { get; set; }
        public string DeployTargetContentComment { get; set; } = "None | SkipCook | Cook";

        public GooglePlayCredential GooglePlayCredential { get; set; }

        public bool IsAllowPublishToGooglePlay { get; set; }
        public bool IsAllowGitCreateTagVersion { get; set; }
        public bool IsAllowGitCreateCommitVersion { get; set; }
        public int GitVersionGenerateFrency { get; set; } = 5;


        public bool SkipGitCheckUpdates { get; set; }
        public bool SkipSvnCheckUpdates { get; set; }
        public bool IsAllowServerVersionIncrement { get; set; }

        public bool IsAllowAutoIncrement { get; set; }
        public string ProjectFilePath { get; set; }
        public string UnrealEnginePath { get; set; }
        public string MasterServerPostDeployGameUrl { get; set; }
        public string MasterServerPostDeployServerUrl { get; set; }

        public string GameServerRepositoryUrl { get; set; }

        public DeployConfiguration ServerConfiguration { get; set; }
        public DeployConfiguration GameConfiguration { get; set; }

    }
}
