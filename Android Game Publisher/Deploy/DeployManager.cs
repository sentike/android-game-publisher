﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AndroidGamePublisher.Deploy.Game;
using AndroidGamePublisher.Deploy.Project;
using AndroidGamePublisher.Deploy.Server;
using AndroidGamePublisher.Extensions;
using AndroidGamePublisher.Version;
using Newtonsoft.Json;
using NLog;

namespace AndroidGamePublisher.Deploy
{
    public class DeployManager
    {
        public static string DeployManagerVersion { get; } = "3.8A at 21.12.2018";

        public DeployManager()
        {
            //======================================
            ProcessThread = new Thread(Process)
            {
                IsBackground = false,
                Priority = ThreadPriority.Highest,
                Name = "Deploy Manager Thread Process"
            };
        }

        private bool ProcessHasCompleted { get; set; }

        private Thread ProcessThread { get; }
        private DeployManifest ProcessManifest { get; set; }
        protected static ILogger Logger { get; } = LogManager.GetCurrentClassLogger();

        private ServerDeployManager ServerDeploy { get; set; }
        private GameDeployManager GameDeploy { get; set; }
        private VersionManager VersionManager { get; set; }
        private DeployOptions DeployOptions { get; set; }
        private Mutex SingleApplicationMutex { get; set; }
        public DateTime StartedDate { get; set; }

        public void Start(DeployOptions options)
        {
            StartedDate = DateTime.Now;
            DeployOptions.Options = options;
            DeployOptions = options;
            ProcessThread.Start();
        }

        private bool ParseDeployManifest(string path)
        {
            //======================================
            Logger.Info($"Starting Parse Deploy Manifest");

            //======================================
            var content = File.ReadAllText(path).Replace("\\", "/");

            //======================================
            try
            {
                //======================================
                ProcessManifest = JsonConvert.DeserializeObject<DeployManifest>(content);

                //======================================
                if (ProcessManifest == null)
                {
                    throw new ArgumentNullException(nameof(ProcessManifest), "Deserialized json object was null");
                }

                //======================================
                return true;
            }
            catch (Exception e)
            {
                Logger.Error($"Failed Parse Deploy Manifest. Exception: {e.Message}");
                Logger.WriteExceptionMessage("Failed Parse Deploy Manifest", e);
                return false;
            }
        }

        private bool LoadDeployManifest()
        {
            //======================================
            var manifestPath = Path.Combine(Directory.GetCurrentDirectory(), "Application", "DeployManifest.json");

            //======================================
            Logger.Info("Starting load deploy manifest ...");
            Logger.Info($"- Manifest Path: {manifestPath}");

            //======================================
            if (File.Exists(manifestPath))
            {
                return ParseDeployManifest(manifestPath);
            }
            else
            {
                CreateDefaultDeployManifest(manifestPath);
                return false;
            }
        }

        private void CreateDefaultDeployManifest(string path)
        {
            Logger.Info("Starting generate default deploy manifest ...");

            //======================================
            var manifest = new DeployManifest
            {
                GooglePlayCredential = new GooglePlayCredential(),
                UnrealEnginePath = @"{PATH_TO_ENGINE}}",
                ProjectFilePath = "{Specify the path to the project folder. Where is the located .UPROJECT file }",
                ServerConfiguration = new DeployConfiguration
                {
                    OutputPath = "{The path to which the builded server will be copied}",
                    ProjectName = "{NAME OF PROJECT, eg PROJECT_NAME.UPROJECT}",
                    BinariesName = "{NAME OF EXECUTABLE FILE}",                   
                },
                GameConfiguration = new DeployConfiguration
                {
                    OutputPath = "{The path to which the builded game will be copied}",
                    ProjectName = "{NAME OF PROJECT, eg PROJECT_NAME.UPROJECT}",
                    BinariesName = "{NAME OF EXECUTABLE FILE}",
                },
            };

            //======================================
            try
            {
                //======================================
                var content = JsonConvert.SerializeObject(manifest, Formatting.Indented);
                Logger.Info("- manifest was successfully generated");

                //======================================
                File.WriteAllText(path, content);

                //======================================
                Logger.Info("- manifest was successfully saved");
            }
            catch (Exception e)
            {
                Logger.Error($"Failed Generate Deploy Manifest: {e.Message}");

                Logger.WriteExceptionMessage("Failed Generate Deploy Manifest", e);
            }
        }

        private void Process()
        {
            //======================================
            Logger.Info("Starting Android Game Publisher ...");
            Logger.Info($"- Version {DeployManagerVersion}");

            //======================================
            SingleApplicationMutex = new Mutex(true, "AndroidGamePublisher", out var createdNew);

            //======================================
            if (createdNew == false || SingleApplicationMutex == null)
            {
                Logger.Fatal("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                Logger.Debug("Android Game Publisher already running");
                Logger.Fatal("Android Game Publisher already running");
                Logger.Warn("Android Game Publisher already running");
                Logger.Info("Android Game Publisher already running");
                Logger.Fatal("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                return;
            }

            //======================================
            if (LoadDeployManifest())
            {
                //======================================
                ProjectDeployManager.UnrealEnginePath = ProcessManifest.UnrealEnginePath;
                ProjectDeployManager.GameProjectPath = ProcessManifest.ProjectFilePath;

                Directory.CreateDirectory("application/logs");
                Directory.CreateDirectory("application/certificates");

                //======================================
                if (DeployOptions.SkipGameDeploy)
                {
                    ProcessManifest.GameConfiguration.DeployProject = false;
                    Logger.Warn("! DeployOptions.SkipGameDeploy has enabled");
                }

                //======================================
                if (DeployOptions.SkipServerDeploy)
                {
                    ProcessManifest.ServerConfiguration.DeployProject = false;
                    Logger.Warn("! DeployOptions.SkipServerDeploy has enabled");
                }

                //======================================
                if (DeployOptions.DeprecateGameVersions)
                {
                    ProcessManifest.GameConfiguration.DeprecatePreviewVersions = true;
                    Logger.Warn("> ! GameConfiguration.DeprecatePreviewVersions has enabled");
                }

                //======================================
                if (DeployOptions.DeprecateServerVersions)
                {
                    ProcessManifest.ServerConfiguration.DeprecatePreviewVersions = true;
                    Logger.Warn("> ! ServerConfiguration.DeprecatePreviewVersions has enabled");
                }

                //======================================
                if (DeployOptions.IsAllowServerVersionIncrement)
                {
                    ProcessManifest.IsAllowServerVersionIncrement = true;
                    Logger.Warn("! GameConfiguration.IsAllowServerVersionIncrement has enabled");
                }
                else
                {
                    ProcessManifest.IsAllowServerVersionIncrement = false;
                    Logger.Warn("! GameConfiguration.IsAllowServerVersionIncrement has disabled");
                }

                //======================================
                if (DeployOptions.IsAllowUpdateGoogleVersionManifest != ProcessManifest.IsAllowUpdateGoogleVersionManifest)
                {
                    ProcessManifest.IsAllowUpdateGoogleVersionManifest = DeployOptions.IsAllowUpdateGoogleVersionManifest;
                    Logger.Warn($"@ Manifest.IsAllowUpdateGoogleVersionManifest[{ProcessManifest.IsAllowUpdateGoogleVersionManifest}] -> {DeployOptions.IsAllowUpdateGoogleVersionManifest}");
                }

                if (ProcessManifest.IsAllowUpdateGoogleVersionManifest)
                {
                    Logger.Debug("! Generate GooglePlayGameVersion.h has enabled");
                }
                else
                {
                    Logger.Error("! Generate GooglePlayGameVersion.h has disabled");
                }

                //======================================
                if (DeployOptions.SkipContentDeploy)
                {
                    ProcessManifest.DeployTargetContent = DeployTargetContent.SkipCook;
                    ProcessManifest.GameConfiguration.DeployTargetContentOverride = DeployTargetContent.SkipCook;
                    ProcessManifest.ServerConfiguration.DeployTargetContentOverride = DeployTargetContent.SkipCook;
                    Logger.Warn("! DeployOptions.SkipContentDeploy has enabled");
                }

                //======================================
                if ((DeployOptions?.DeployTarget ?? DeployTarget.None) != DeployTarget.None)
                {
                    Logger.Warn($"! DeployOptions.DeployTarget | switch {ProcessManifest.DeployTarget} to {DeployOptions?.DeployTarget}");
                    ProcessManifest.DeployTarget = DeployOptions.DeployTarget;
                }

                //======================================
                if (DeployOptions.IsAllowPublishToGooglePlay)
                {
                    ProcessManifest.IsAllowPublishToGooglePlay = true;
                }

                //======================================
                VersionManager = new VersionManager(ProcessManifest);

                //======================================
                ServerDeploy = new ServerDeployManager(ProcessManifest, VersionManager);
                GameDeploy = new GameDeployManager(ProcessManifest, VersionManager);

                //======================================
                if (ProcessManifest.GooglePlayCredential == null)
                {
                    Logger.Error("! Failed load GooglePlayCredential!");
                }
                else
                {
                    if (ProcessManifest.GameConfiguration.DeployProject)
                    {
                        ProcessManifest.GooglePlayCredential.Process();
                    }
                }

                //======================================
                if (VersionManager.Process())
                {
                    if (ServerDeploy.Process())
                    {
                        GameDeploy.Process();
                    }
                }
            }
            else
            {
                Logger.Warn("! Please configure your DeployManifest.json !");
                Logger.Error("! Please configure your DeployManifest.json !");
            }

            //======================================
            ProcessHasCompleted = true;
        }


        public void Wait()
        {
            //======================================
            while (ProcessHasCompleted == false || ProcessThread.IsAlive)
            {
                Thread.Sleep(250);
            }

            //======================================
            var EndDate = DateTime.Now;

            //======================================
            LogManager.GetCurrentClassLogger().Warn($"Building started at {StartedDate}");
            LogManager.GetCurrentClassLogger().Warn($"Building completed at {EndDate}");
            LogManager.GetCurrentClassLogger().Debug($"Building elapsed at {EndDate - StartedDate}");

            //======================================
          

            LogManager.GetCurrentClassLogger().Warn("Application terminated. Press <enter> to exit...");
            Console.ReadLine();
        }
    }
}
