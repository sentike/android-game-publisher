﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;

namespace AndroidGamePublisher.Deploy
{
    public class DeployOptions
    {
        public static DeployOptions Options { get; set; }

        [Option(HelpText = "Skip game deploy?")]
        public bool SkipGameDeploy { get; set; }

        [Option(HelpText = "Skip server deploy?")]
        public bool SkipServerDeploy { get; set; }

        [Option(HelpText = "Skip content cook?")]
        public bool SkipContentDeploy { get; set; }

        [Option(HelpText = "Deprecate old Game Versions?")]
        public bool DeprecateGameVersions { get; set; }

        [Option(HelpText = "Deprecate old Server Versions?")]
        public bool DeprecateServerVersions { get; set; }

        [Option(HelpText = "Generate new Server Version?")]
        public bool IsAllowServerVersionIncrement { get; set; }

        [Option(HelpText = "Publish game to Google Play?")]
        public bool IsAllowPublishToGooglePlay { get; set; }

        [Option(HelpText = "Generate GooglePlayGameVersion.h ?")]
        public bool IsAllowUpdateGoogleVersionManifest { get; set; }


        [Option('m', "message", HelpText = "Publish game to Google Play?")]
        public string Message { get; set; }


        [Value(0, MetaName = "Internal | Alpha | Beta | Production", HelpText = "Google Play Deploy Target: Internal | Alpha | Beta | Production")]
        public DeployTarget DeployTarget { get; set; }
    }
}
