﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidGamePublisher.Extensions;
using AndroidGamePublisher.Version;
using Google.Apis.AndroidPublisher.v3;
using Google.Apis.AndroidPublisher.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using NLog;

namespace AndroidGamePublisher.Deploy.Game
{
    public class GooglePlayGameDeploy
    {
        private DeployManifest DeployManifest { get; }
        private GooglePlayCredential CredentialManifest { get; }
        private ServiceAccountCredential Credential { get; set; }
        private AndroidPublisherService PublisherService { get; set; }
        private EditsResource Edits { get; set; }

        private static ILogger Logger { get; } = LogManager.GetCurrentClassLogger();

        private bool bContentHasCoocked { get; }
        private string DeployTrack { get; }
        private string ProjectName { get; }
        private string PackageName { get; }
        private string TransactionId { get; set; }

        public string ApkFileLocation { get; set; }
        public string ObbFileLocation { get; set; }

        private int GameStoreVersion => VersionManager?.NextStoreVersion ?? -1;

        private VersionManager VersionManager { get; }

        public GooglePlayGameDeploy(DeployManifest manifest, DeployConfiguration configuration, VersionManager version)
        {
            //=======================================
            bContentHasCoocked = manifest.DeployTargetContent == DeployTargetContent.Cook || configuration.DeployTargetContentOverride == DeployTargetContent.Cook;

            //=======================================
            DeployManifest = manifest;
            VersionManager = version;
            CredentialManifest = DeployManifest.GooglePlayCredential;

            //=======================================
            ProjectName = configuration.ProjectName;
            PackageName = configuration.BoundleName;

            //=======================================
            DeployTrack = manifest.DeployTarget.ToString().ToLower();

            //=======================================
        }

        private void Authorization()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Credential loading ...");

            //==============================================================
            Credential = new ServiceAccountCredential
            (
                new ServiceAccountCredential.Initializer(CredentialManifest.ServiceAccountEmail)
                {
                    Scopes = new[] { AndroidPublisherService.Scope.Androidpublisher }
                }.FromCertificate(CredentialManifest.Certificate)
            );

            //==============================================================
            Logger.Debug($"[GooglePlay] Credential loaded");
        }

        private void StartAndroidPublisherService()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp start client ...");

            //==============================================================
            PublisherService = new AndroidPublisherService(new BaseClientService.Initializer
            {
                ApiKey = CredentialManifest.ApiKey,
                ApplicationName = CredentialManifest.ApplicationName,
                HttpClientInitializer = Credential,
            });

            //==============================================================
            Logger.Debug($"[GooglePlay] Start client successfully");
        }

        private bool StartEditTransactionProxy(int Attemp, int AttempCount)
        {
            try
            {
                //==============================================================
                Logger.Info($"[GooglePlay] Attemp {Attemp} / {AttempCount} create EditsResource ...");
                Edits = PublisherService.Edits;
                Logger.Debug($"[GooglePlay] Create EditsResource successfully");

                //==============================================================
                Logger.Info($"[GooglePlay] Attemp {Attemp} / {AttempCount} start Transaction ...");

                var Transaction = Edits.Insert(null, PackageName).Execute();
                Logger.Debug($"[GooglePlay] start Transaction successfully");

                //==============================================================
                TransactionId = Transaction.Id;

                //==============================================================
                Logger.Info($"[GooglePlay] TransactionId: {TransactionId}");

                //==============================================================
                return true;
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"[StartEditTransaction][Attemp: {Attemp} / {AttempCount}]", e, false);
                if (Attemp >= AttempCount) throw e;
                return false;
            }
        }


        private void StartEditTransaction()
        {
            var AttempCount = 5;
            for (int i = 1; i <= AttempCount; i++)
            {
                if (StartEditTransactionProxy(i, AttempCount)) break;
            }
        }

        private bool CommitChangesProxy(int Attemp, int AttempCount)
        {
            try
            {
                //==============================================================
                Logger.Info($"[GooglePlay] Attemp {Attemp} / {AttempCount} Commit changes ...");

                //==============================================================
                Edits.Commit(PackageName, TransactionId).Execute();

                //==============================================================
                Logger.Debug($"[GooglePlay] Commit changes successfully");

                //==============================================================
                return true;
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"[CommitChangesProxy][Attemp: {Attemp} / {AttempCount}]", e, false);
                if (Attemp >= AttempCount) throw e;
                return false;
            }
        }


        private void CommitChanges()
        {
            var AttempCount = 5;
            for (int i = 1; i <= AttempCount; i++)
            {
                if (CommitChangesProxy(i, AttempCount)) break;
            }
        }

        private void PublishProxy()
        {
            //==============================================================
            Logger.Info($"[GooglePlay] PackageName: {PackageName}");
            Logger.Info($"[GooglePlay] GameStoreVersion: {GameStoreVersion}");

            //==============================================================
            Authorization();

            //==============================================================
            StartAndroidPublisherService();

            //==============================================================
            StartEditTransaction();

            //==============================================================
            PublishApk();
            PublishObb();

            //==============================================================
            SwitchTracks();

            //==============================================================
            CommitChanges();
        }

        public void Publish()
        {
            Logger.Warn($"============================================================");
            Logger.Info($"[GooglePlay] Prepare to GooglePlay publish : {DeployTrack} ...");

            //==============================================================
            try
            {
                PublishProxy();
            }
            catch(Exception e)
            {         
                //==============================================================
                Logger.Error($"============================================================");
                Logger.Error($"[GooglePlay] Failed to publish the application to Google Play");
                Logger.Error($"[GooglePlay] Publish APK and OBB manually");
                Logger.Error($"============================================================");

                //==============================================================
                Project.ProjectDeployManager.StartProcess(Path.GetDirectoryName(ApkFileLocation), String.Empty);
                Logger.WriteExceptionMessage($"[GooglePlay][Publish][{DeployTrack}][GameStoreVersion: {GameStoreVersion}]", e);
            }
        }

        private IList<Apk> GetApkList(bool InDisplayList = false)
        {
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp get APK list ...");

            //==============================================================
            var Response = Edits.Apks.List(PackageName, TransactionId).Execute();

            //==============================================================
            Logger.Info($"[GooglePlay] Getted APK list: {Response.Apks.Count}");

            //==============================================================
            if (InDisplayList)
            {
                foreach (var apk in Response.Apks)
                {
                    Logger.Debug($"> {apk.VersionCode} | Sha1: {apk.Binary.Sha1} | ETag: {apk.ETag}");
                }
            }

            //==============================================================
            return Response.Apks;
        }

        public string TrackReleaseStatus = "completed";

        public long GetLastApkVersion(DeployTarget InDeployTarget)
        {         
            //==============================================================
            var TargetTrackValue = InDeployTarget.ToString().ToLower();

            //==============================================================
            var TrackList = GetTrackList(true);

            //==============================================================
            var TargetTrack = TrackList.SingleOrDefault(p => p.TrackValue == TargetTrackValue);

            //==============================================================
            var TargetTrackReleases = TargetTrack.Releases;

            //==============================================================
            var CompletedTrackReleases = TargetTrackReleases.Where(p => p.Status == TrackReleaseStatus).ToArray();

            //==============================================================
            var SortedTrackReleases = CompletedTrackReleases.OrderBy(p => p.VersionCodes[0]).ToArray();

            //==============================================================
            var FirstTrackRelease = SortedTrackReleases.FirstOrDefault();
            var LastTrackRelease = SortedTrackReleases.LastOrDefault();

            //==============================================================
            if (FirstTrackRelease != null)
            {
                Logger.Info($"[GooglePlay] FirstTrackRelease: {string.Join(",", FirstTrackRelease.VersionCodes)}");
            }

            if (LastTrackRelease != null)
            {
                Logger.Info($"[GooglePlay] LastTrackRelease: {string.Join(",", LastTrackRelease.VersionCodes)}");
            }

            //==============================================================
            return LastTrackRelease.VersionCodes[0] ?? -1;
        }

        private IList<Track> GetTrackList(bool InDisplayList = false)
        {       
            //==============================================================
            Logger.Info($"[GooglePlay] Attemp get Track list ...");

            //==============================================================
            var Response = Edits.Tracks.List(PackageName, TransactionId).Execute();

            //==============================================================
            Logger.Info($"[GooglePlay] Getted Track list: {Response.Tracks.Count}");

            //==============================================================
            if (InDisplayList)
            {
                foreach (var track in Response.Tracks)
                {
                    var releases = track.Releases.Select(p => $"{p.Name} -> {p.Status}").ToArray();
                    Logger.Debug($"> {track.TrackValue} | Releases: {string.Join(",", releases)}");
                }
            }

            //==============================================================
            return Response.Tracks;
        }


        bool PublishApkProxy(int Attemp, int AttempCount)
        {
            try
            { 
                Logger.Info($"[GooglePlay] Attemp {Attemp} / {AttempCount} read APK file ...");
                Logger.Info($"[GooglePlay] ApkFileLocation: {ApkFileLocation}");

                using (var fs = new FileStream(ApkFileLocation, FileMode.Open))
                {
                    Logger.Warn($"[GooglePlay] Attemp {Attemp} / {AttempCount} upload APK file ...");

                    var UploadRequest = Edits.Apks.Upload
                    (
                        PackageName,
                        TransactionId,
                        fs,
                        "application/vnd.android.package-archive"
                    );

                    UploadRequest.ProgressChanged += progress => OnUploadProgressChanged("APK", progress, fs.Length);
                    UploadRequest.UploadAsync().Wait();

                    Logger.Debug($"[GooglePlay] Upload APK file successfully");
                }
                //==============================================================
                return true;
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"[PublishApkProxy][Attemp: {Attemp} / {AttempCount}]", e, false);
                if (Attemp >= AttempCount) throw e;
                return false;
            }
        }

        private void PublishApk()
        {
            //GetApkList();
            //GetTrackList();
            var AttempCount = 5;
            for (int i = 1; i <= AttempCount; i++)
            {
                if (PublishApkProxy(i, AttempCount)) break;
            }

        }

        private bool UploadObbProxy(int Attemp, int AttempCount)
        {
            try
            {
                Logger.Info($"[GooglePlay] Attemp read OBB file ...");
                Logger.Info($"[GooglePlay] ObbFileLocation: {ObbFileLocation}");

                using (var fs = new FileStream(ObbFileLocation, FileMode.Open))
                {
                    Logger.Warn($"[GooglePlay] Attemp {Attemp} / {AttempCount} upload OBB file ...");

                    var UploadRequest = Edits.Expansionfiles.Upload
                    (
                        PackageName,
                        TransactionId,
                        GameStoreVersion,
                        EditsResource.ExpansionfilesResource.UploadMediaUpload.ExpansionFileTypeEnum.Main, fs,
                        "application/octet-stream"
                    );

                    UploadRequest.ProgressChanged += progress => OnUploadProgressChanged("OBB", progress, fs.Length);
                    UploadRequest.UploadAsync().Wait();

                    Logger.Debug($"[GooglePlay] Upload OBB file successfully");
                }

                //==============================================================
                return true;
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"[UploadObbProxy][Attemp: {Attemp} / {AttempCount}]", e, false);
                if (Attemp >= AttempCount) throw e;
                return false;
            }
        }

        private void UploadObb()
        {
            var AttempCount = 5;
            for (int i = 1; i <= AttempCount; i++)
            {
                if (UploadObbProxy(i, AttempCount)) break;
            }
        }

        private void OnUploadProgressChanged(string action, IUploadProgress progress, long total)
        {
            if (progress.Status == UploadStatus.Completed)
            {
                Logger.Debug($"> [{action}] {progress.Status} | BytesSent: {progress.BytesSent} of {total}");
            }
            else if (progress.Status == UploadStatus.Failed)
            {
                Logger.WriteExceptionMessage($"> [{action}] {progress.Status} | BytesSent: {progress.BytesSent}", progress.Exception);
            }
            else
            {
                Logger.Warn($"> [{action}] {progress.Status} | BytesSent: {progress.BytesSent} of {total} | {progress.BytesSent * 100 / total}%");
            }
        }

        private void UseLastObb()
        {
            //==============================================================
            var LastApkVersion = (int) GetLastApkVersion(DeployManifest.DeployTarget);

            //==============================================================
            Logger.Info($"[GooglePlay][UseLastObb] LastApkVersion: {LastApkVersion}");

            //==============================================================
            var TargetExpansionfile = Edits.Expansionfiles.Get
            (
                PackageName,
                TransactionId,
                LastApkVersion,  
                EditsResource.ExpansionfilesResource.GetRequest.ExpansionFileTypeEnum.Main
            ).Execute();

            //==============================================================
            Logger.Info($"[GooglePlay][UseLastObb] TargetExpansionfile.FileSize : {TargetExpansionfile.FileSize}Bytes / {TargetExpansionfile.FileSize / 1024 / 1024}Mb");

            //==============================================================
            Logger.Warn($"[GooglePlay][UseLastObb] Prepare update expansionfiles");

            //==============================================================
            Edits.Expansionfiles.Update(new ExpansionFile()
            {
                ReferencesVersion = LastApkVersion
            }, PackageName, TransactionId, GameStoreVersion, EditsResource.ExpansionfilesResource.UpdateRequest.ExpansionFileTypeEnum.Main).Execute();

            //==============================================================
            Logger.Debug($"[GooglePlay][UseLastObb] Update expansionfiles successfully");
        }

        private void PublishObb()
        {
            Logger.Info($"=============================================");
            Logger.Warn($"[GooglePlay] Prepare to Publish Obb");
            Logger.Warn($"[GooglePlay] bContentHasCoocked: {bContentHasCoocked}");
            Logger.Info($"=============================================");

            if (bContentHasCoocked || true)
            {
                Logger.Debug($"[GooglePlay] Prepare to UploadObb");
                UploadObb();
            }
            else
            {
                try
                {
                    Logger.Debug($"[GooglePlay] Prepare to UseLastObb");
                    UseLastObb();
                }
                catch(Exception e)
                {
                    Logger.WriteExceptionMessage($"[GooglePlay] Failed UseLastObb", e, false);
                    Logger.Debug($"[GooglePlay] Prepare to UploadObb");
                    UploadObb();
                }
            }
        }

        private string GameStoreVersionString
        {
            get
            {
                if (DeployManifest.DeployTarget == DeployTarget.Alpha) return $"{GameStoreVersion}A";
                if (DeployManifest.DeployTarget == DeployTarget.Beta) return $"{GameStoreVersion}B";
                if (DeployManifest.DeployTarget == DeployTarget.Internal) return $"{GameStoreVersion}D";
                return GameStoreVersion.ToString();
            }
        }

        private bool SwitchTracksProxy(int Attemp, int AttempCount)
        {
            try
            {         
                //==============================================================
                var message = new TrackRelease();

                //==============================================================
                Logger.Info($"[GooglePlay] Attemp {Attemp} / {AttempCount} switch Tracks to {DeployTrack} | GameStoreVersion: {GameStoreVersionString} ...");

                //======================================
                var MessageRussian = $"Автоматическое обновление v{GameStoreVersionString} от {DateTime.Now} {Environment.NewLine}" + VersionManager.GetDeployMessage(Languages.Russian);
                var MessageEnglish = $"Automation bild v{GameStoreVersionString} at {DateTime.Now} {Environment.NewLine}" + VersionManager.GetDeployMessage(Languages.English);

                //======================================
                message.Name = $"{ProjectName} {DeployTrack} v{GameStoreVersionString}";
                message.Status = "completed";
                message.VersionCodes = new List<long?> { GameStoreVersion };
                message.ReleaseNotes = new List<LocalizedText>
                {
                    new LocalizedText
                    {
                        Language = "ru-RU",
                        Text = MessageRussian.Substring(0, Math.Min(500, MessageRussian.Length))
                    },
                    new LocalizedText
                    {
                        Language = "en-US",
                        Text =  MessageEnglish.Substring(0, Math.Min(500, MessageEnglish.Length))
                    }
                };

                var track = new Track()
                {
                    Releases = new List<TrackRelease>() { message }
                };

                Edits.Tracks.Update(track, PackageName, TransactionId, DeployTrack).Execute();
                Logger.Debug($"[GooglePlay] Switch Tracks to {DeployTrack} successfully");

                //==============================================================
                return true;
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"[SwitchTracksProxy][Attemp: {Attemp} / {AttempCount}]", e, false);
                if (Attemp >= AttempCount) throw e;
                return false;
            }
        }

        private void SwitchTracks()
        {
            var AttempCount = 5;
            for (int i = 1; i <= AttempCount; i++)
            {
                if (SwitchTracksProxy(i, AttempCount)) break;
            }
        }
 
    }
}
