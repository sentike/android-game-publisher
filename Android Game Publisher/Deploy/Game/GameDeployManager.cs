﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AndroidGamePublisher.Deploy.Project;
using AndroidGamePublisher.Extensions;
using AndroidGamePublisher.Version;
using CliWrap;
using CliWrap.Models;
using CliWrap.Services;
using Google.Apis.AndroidPublisher.v3;
using Google.Apis.AndroidPublisher.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Newtonsoft.Json;
using RestSharp;

namespace AndroidGamePublisher.Deploy.Game
{
    public class GameDeployManager : ProjectDeployManager
    {
        public override DeployConfiguration Configuration => DeployManifest?.GameConfiguration;
        public GooglePlayGameDeploy GooglePlayDeloyManager { get; }

        public GameDeployManager(DeployManifest manifest, VersionManager versionManager) : base(manifest, versionManager)
        {
            GooglePlayDeloyManager= new GooglePlayGameDeploy(manifest, Configuration, versionManager);
        }

        protected override void DeployProcess()
        {
            Logger.Info($"- TextureCompression: {DeployManifest.TextureCompression}");
            Logger.Info($"- IsAllowPublishToGooglePlay: {DeployManifest.IsAllowPublishToGooglePlay}");

            RunUAT(new[]
            {
                $"-cookflavor={DeployManifest.TextureCompression}"
            });
        }

        protected override void ProcessDeleteLatestDeploy()
        {
            //===================================================
            Logger.Info($"- Prepare to delete {AndroidRootPath}");

            //===================================================
            try
            {
                if (Directory.Exists(AndroidRootPath))
                {
                    Directory.Delete(AndroidRootPath, true);
                }
            }
            catch (Exception e)
            {
                Logger.Warn($"- Failed delete Android_{DeployManifest.TextureCompression}. Path: {AndroidRootPath} | Exception: {e.Message}");
            }
        }

        protected override bool IsExistsGameContent()
        {
            var bIsExistsGameBinaries = File.Exists(ObbFilePath);
            Logger.Info($"- IsExistsGameBinaries: {bIsExistsGameBinaries} | path: {ObbFilePath}");
            return bIsExistsGameBinaries;
        }

        public string AndroidRootPath => Path.Combine(Configuration.OutputPath, $"Android_{DeployManifest.TextureCompression}");
        public string ObbFilePath => Path.Combine(AndroidRootPath, $"main.{VersionManager.NextStoreVersion}.{Configuration.BoundleName}.obb");
        public string ApkFilePath => Path.Combine(AndroidRootPath, $"{Configuration.BinariesName}{GetClientConfig(Configuration.ClientConfig)}-{GetPlatformBitDepth(Configuration.PlatformBitDepth)}-es2.apk");

        public override string GetPlatformBitDepth(ApplicationPlatformBitDepth platformBitDepth)
        {
            if (platformBitDepth == ApplicationPlatformBitDepth.X64)
            {
                return "arm64";
            }
            else if (platformBitDepth == ApplicationPlatformBitDepth.X86)
            {
                return "armv7";
            }
            throw new NotImplementedException("Not implemented mixed PlatformBitDepth");
        }

        protected override bool IsExistsGameBinaries()
        {
            var bIsExistsGameBinaries = File.Exists(ApkFilePath);
            Logger.Info($"- IsExistsGameBinaries: {bIsExistsGameBinaries} | path: {ApkFilePath}");
            return bIsExistsGameBinaries;
        }

        protected override void PostDeployProcess()
        {
            //=================================
            GooglePlayDeloyManager.ApkFileLocation = ApkFilePath;
            GooglePlayDeloyManager.ObbFileLocation = ObbFilePath;
            GooglePlayDeloyManager.Publish();

            //=================================
            PostDeployMasterServerNotify(VersionManager.NextGameVersionModel, DeployManifest.MasterServerPostDeployGameUrl);

            //=================================
            try
            {
                if (DeployManifest.IsAllowGitCreateCommitVersion)
                {
                    using (var cli = new Cli("git", new CliSettings
                    {
                        WorkingDirectory = GameRootPath
                    }))
                    {
                        cli.Execute($"commit -m\"Automation build {VersionManager.NextVersionDisplayName} at {DeployManifest.DeployTarget} | TargetServerVersion: {VersionManager.TargetServerVersion} -> {VersionManager.NextTargetServerVersion}\"");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed git version commit", e, false);
            }

            //=================================
            try
            {
                if (DeployManifest.IsAllowGitCreateTagVersion)
                {
                    if (VersionManager.NextStoreVersion % DeployManifest.GitVersionGenerateFrency == 0)
                    {
                        using (var cli = new Cli("git", new CliSettings
                        {
                            WorkingDirectory = GameRootPath
                        }))
                        {
                            cli.Execute($"tag -a \"GameV{VersionManager.NextVersionDisplayName}\" -m \"Build {VersionManager.NextStoreVersion}\"");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteExceptionMessage($"- Failed git version tag", e, false);
            }
        }
    }
}
