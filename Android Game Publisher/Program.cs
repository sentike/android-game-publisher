﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidGamePublisher.Deploy;
using CommandLine;

namespace AndroidGamePublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new DeployManager();
            Parser.Default.ParseArguments<DeployOptions>(args).WithParsed(o => manager.Start(o));
            manager.Wait();
        }
    }
}
