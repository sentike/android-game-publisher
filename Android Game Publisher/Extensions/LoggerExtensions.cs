﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace AndroidGamePublisher.Extensions
{
    public static class LoggerExtensions
    {
        public static void WriteExceptionMessage(this ILogger logger, string description, Exception exception, bool isFatal = true)
        {
            if (exception == null)
            {
                logger.Error($"{description} {Environment.NewLine}");
            }
            else
            {
                var sb = new StringBuilder(4096);
                sb.AppendLine(exception.ToString());

                var innerEx = exception.InnerException;
                while (innerEx != null)
                {
                    sb.Append($"Inner Exception : {innerEx.Message} {Environment.NewLine}");
                    innerEx = innerEx.InnerException;
                }

                if (isFatal)
                {
                    logger.Fatal($"{description} | exception: {sb} {Environment.NewLine}");
                }
                else
                {
                    logger.Error($"{description} | exception: {sb} {Environment.NewLine}");
                }
            }
        }
    }
}
