﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndroidGamePublisher.Extensions
{
    public class TimeWatchDog
    {
        public DateTime? StartedDate { get; private set; }
        public DateTime? CompleteDate { get; private set; }
        public TimeSpan? Elapsed => CompleteDate - StartedDate;

        public void Reset()
        {
            StartedDate = null;
            CompleteDate = null;
        }

        public void Start()
        {
            StartedDate = DateTime.UtcNow;
        }

        public void Stop()
        {
            CompleteDate = DateTime.UtcNow;
        }
    }

    //public class TimeWatchDogContainer : TimeWatchDog, IDisposable
    //{
    //    public TimeWatchDogContainer(out int q)
    //    {
    //        q = 0;
    //    }
    //
    //    public void Dispose()
    //    {
    //    }
    //}

}
