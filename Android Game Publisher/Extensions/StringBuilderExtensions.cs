﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndroidGamePublisher.Extensions
{
    public static class StringBuilderExtensions
    {
        public static void Add(this StringBuilder sb, string key, string splitter = "-")
        {
            sb.Append($"{splitter}{key} ");

        }

        public static void AddPair(this StringBuilder sb, string key, object value, string splitter = "-")
        {
            sb.Append($"{splitter}{key}={value} ");
        }

        public static void AddEscaped(this StringBuilder sb, string key, object value, string splitter = "-")
        {
            sb.Append($"{splitter}{key}=\"{value}\" ");
        }
    }
}
