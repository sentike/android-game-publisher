﻿namespace AndroidPublisherConfigurator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rRelease = new System.Windows.Forms.RadioButton();
            this.rBeta = new System.Windows.Forms.RadioButton();
            this.rAlpha = new System.Windows.Forms.RadioButton();
            this.rDev = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cPublishGooglePlay = new System.Windows.Forms.CheckBox();
            this.cCookContent = new System.Windows.Forms.CheckBox();
            this.cBuildServer = new System.Windows.Forms.CheckBox();
            this.cBuildGame = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cGooglePlayGameVersion = new System.Windows.Forms.CheckBox();
            this.cAIServerVersion = new System.Windows.Forms.CheckBox();
            this.cDeprecateServerVersions = new System.Windows.Forms.CheckBox();
            this.cDeprecateGameVersions = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tWhatNews = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.bHotFix = new System.Windows.Forms.Button();
            this.bOpt = new System.Windows.Forms.Button();
            this.bNewFunc = new System.Windows.Forms.Button();
            this.lLenghtOfMax = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rRelease);
            this.groupBox1.Controls.Add(this.rBeta);
            this.groupBox1.Controls.Add(this.rAlpha);
            this.groupBox1.Controls.Add(this.rDev);
            this.groupBox1.Location = new System.Drawing.Point(18, 19);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(232, 171);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ветка";
            // 
            // rRelease
            // 
            this.rRelease.AutoSize = true;
            this.rRelease.Location = new System.Drawing.Point(8, 130);
            this.rRelease.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rRelease.Name = "rRelease";
            this.rRelease.Size = new System.Drawing.Size(73, 24);
            this.rRelease.TabIndex = 4;
            this.rRelease.TabStop = true;
            this.rRelease.Text = "Релиз";
            this.rRelease.UseVisualStyleBackColor = true;
            // 
            // rBeta
            // 
            this.rBeta.AutoSize = true;
            this.rBeta.Location = new System.Drawing.Point(8, 97);
            this.rBeta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rBeta.Name = "rBeta";
            this.rBeta.Size = new System.Drawing.Size(65, 24);
            this.rBeta.TabIndex = 3;
            this.rBeta.TabStop = true;
            this.rBeta.Text = "Бета";
            this.rBeta.UseVisualStyleBackColor = true;
            // 
            // rAlpha
            // 
            this.rAlpha.AutoSize = true;
            this.rAlpha.Location = new System.Drawing.Point(8, 63);
            this.rAlpha.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rAlpha.Name = "rAlpha";
            this.rAlpha.Size = new System.Drawing.Size(81, 24);
            this.rAlpha.TabIndex = 2;
            this.rAlpha.TabStop = true;
            this.rAlpha.Text = "Альфа";
            this.rAlpha.UseVisualStyleBackColor = true;
            // 
            // rDev
            // 
            this.rDev.AutoSize = true;
            this.rDev.Checked = true;
            this.rDev.Location = new System.Drawing.Point(8, 29);
            this.rDev.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rDev.Name = "rDev";
            this.rDev.Size = new System.Drawing.Size(219, 24);
            this.rDev.TabIndex = 1;
            this.rDev.TabStop = true;
            this.rDev.Text = "Внутренее тестирование";
            this.rDev.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cPublishGooglePlay);
            this.groupBox2.Controls.Add(this.cCookContent);
            this.groupBox2.Controls.Add(this.cBuildServer);
            this.groupBox2.Controls.Add(this.cBuildGame);
            this.groupBox2.Location = new System.Drawing.Point(257, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 253);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Сборка";
            // 
            // cPublishGooglePlay
            // 
            this.cPublishGooglePlay.AutoSize = true;
            this.cPublishGooglePlay.Checked = true;
            this.cPublishGooglePlay.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cPublishGooglePlay.Location = new System.Drawing.Point(6, 130);
            this.cPublishGooglePlay.Name = "cPublishGooglePlay";
            this.cPublishGooglePlay.Size = new System.Drawing.Size(146, 24);
            this.cPublishGooglePlay.TabIndex = 3;
            this.cPublishGooglePlay.Text = "Опубликовать?";
            this.toolTip1.SetToolTip(this.cPublishGooglePlay, "Опубликовать автоматически в Google Play?");
            this.cPublishGooglePlay.UseVisualStyleBackColor = true;
            // 
            // cCookContent
            // 
            this.cCookContent.AutoSize = true;
            this.cCookContent.Checked = true;
            this.cCookContent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cCookContent.Location = new System.Drawing.Point(6, 98);
            this.cCookContent.Name = "cCookContent";
            this.cCookContent.Size = new System.Drawing.Size(168, 24);
            this.cCookContent.TabIndex = 2;
            this.cCookContent.Text = "Собирать контент";
            this.cCookContent.UseVisualStyleBackColor = true;
            // 
            // cBuildServer
            // 
            this.cBuildServer.AutoSize = true;
            this.cBuildServer.Checked = true;
            this.cBuildServer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBuildServer.Location = new System.Drawing.Point(7, 68);
            this.cBuildServer.Name = "cBuildServer";
            this.cBuildServer.Size = new System.Drawing.Size(159, 24);
            this.cBuildServer.TabIndex = 1;
            this.cBuildServer.Text = "Собирать сервер";
            this.cBuildServer.UseVisualStyleBackColor = true;
            this.cBuildServer.CheckedChanged += new System.EventHandler(this.cBuildServer_CheckedChanged);
            // 
            // cBuildGame
            // 
            this.cBuildGame.AutoSize = true;
            this.cBuildGame.Checked = true;
            this.cBuildGame.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cBuildGame.Location = new System.Drawing.Point(7, 35);
            this.cBuildGame.Name = "cBuildGame";
            this.cBuildGame.Size = new System.Drawing.Size(138, 24);
            this.cBuildGame.TabIndex = 0;
            this.cBuildGame.Text = "Собирать игру";
            this.cBuildGame.UseVisualStyleBackColor = true;
            this.cBuildGame.CheckedChanged += new System.EventHandler(this.cBuildGame_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cGooglePlayGameVersion);
            this.groupBox3.Controls.Add(this.cAIServerVersion);
            this.groupBox3.Controls.Add(this.cDeprecateServerVersions);
            this.groupBox3.Controls.Add(this.cDeprecateGameVersions);
            this.groupBox3.Location = new System.Drawing.Point(463, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(286, 206);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Управление версиями";
            // 
            // cGooglePlayGameVersion
            // 
            this.cGooglePlayGameVersion.AutoSize = true;
            this.cGooglePlayGameVersion.Checked = true;
            this.cGooglePlayGameVersion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cGooglePlayGameVersion.Location = new System.Drawing.Point(6, 97);
            this.cGooglePlayGameVersion.Name = "cGooglePlayGameVersion";
            this.cGooglePlayGameVersion.Size = new System.Drawing.Size(279, 24);
            this.cGooglePlayGameVersion.TabIndex = 3;
            this.cGooglePlayGameVersion.Text = "Генерировать новый манифест?";
            this.cGooglePlayGameVersion.UseVisualStyleBackColor = true;
            // 
            // cAIServerVersion
            // 
            this.cAIServerVersion.AutoSize = true;
            this.cAIServerVersion.Checked = true;
            this.cAIServerVersion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cAIServerVersion.Location = new System.Drawing.Point(6, 130);
            this.cAIServerVersion.Name = "cAIServerVersion";
            this.cAIServerVersion.Size = new System.Drawing.Size(238, 24);
            this.cAIServerVersion.TabIndex = 2;
            this.cAIServerVersion.Text = "Обновить версию сервера?";
            this.cAIServerVersion.UseVisualStyleBackColor = true;
            // 
            // cDeprecateServerVersions
            // 
            this.cDeprecateServerVersions.AutoSize = true;
            this.cDeprecateServerVersions.Location = new System.Drawing.Point(7, 68);
            this.cDeprecateServerVersions.Name = "cDeprecateServerVersions";
            this.cDeprecateServerVersions.Size = new System.Drawing.Size(277, 24);
            this.cDeprecateServerVersions.TabIndex = 1;
            this.cDeprecateServerVersions.Text = "Удалить старые версии сервера";
            this.cDeprecateServerVersions.UseVisualStyleBackColor = true;
            // 
            // cDeprecateGameVersions
            // 
            this.cDeprecateGameVersions.AutoSize = true;
            this.cDeprecateGameVersions.Location = new System.Drawing.Point(7, 35);
            this.cDeprecateGameVersions.Name = "cDeprecateGameVersions";
            this.cDeprecateGameVersions.Size = new System.Drawing.Size(251, 24);
            this.cDeprecateGameVersions.TabIndex = 0;
            this.cDeprecateGameVersions.Text = "Удалить старые версии игры";
            this.cDeprecateGameVersions.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(463, 231);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(286, 41);
            this.button1.TabIndex = 4;
            this.button1.Text = "Запустить сборку";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tWhatNews
            // 
            this.tWhatNews.Location = new System.Drawing.Point(18, 308);
            this.tWhatNews.MaxLength = 470;
            this.tWhatNews.Name = "tWhatNews";
            this.tWhatNews.Size = new System.Drawing.Size(729, 242);
            this.tWhatNews.TabIndex = 5;
            this.tWhatNews.Text = "";
            this.tWhatNews.TextChanged += new System.EventHandler(this.tWhatNews_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Что нового?";
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "0";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Русский",
            "Английский",
            "Немецкий"});
            this.comboBox1.Location = new System.Drawing.Point(127, 278);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 28);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.Text = "Русский";
            this.comboBox1.ValueMember = "0";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // bHotFix
            // 
            this.bHotFix.Location = new System.Drawing.Point(18, 556);
            this.bHotFix.Name = "bHotFix";
            this.bHotFix.Size = new System.Drawing.Size(202, 30);
            this.bHotFix.TabIndex = 0;
            this.bHotFix.Text = "Исправление ошибок";
            this.bHotFix.UseVisualStyleBackColor = true;
            this.bHotFix.Click += new System.EventHandler(this.bHotFix_Click);
            // 
            // bOpt
            // 
            this.bOpt.Location = new System.Drawing.Point(226, 556);
            this.bOpt.Name = "bOpt";
            this.bOpt.Size = new System.Drawing.Size(241, 30);
            this.bOpt.TabIndex = 1;
            this.bOpt.Text = "Оптимизация игры";
            this.bOpt.UseVisualStyleBackColor = true;
            this.bOpt.Click += new System.EventHandler(this.bOpt_Click);
            // 
            // bNewFunc
            // 
            this.bNewFunc.Location = new System.Drawing.Point(476, 556);
            this.bNewFunc.Name = "bNewFunc";
            this.bNewFunc.Size = new System.Drawing.Size(274, 30);
            this.bNewFunc.TabIndex = 2;
            this.bNewFunc.Text = "Новый функционал";
            this.bNewFunc.UseVisualStyleBackColor = true;
            this.bNewFunc.Click += new System.EventHandler(this.bNewFunc_Click);
            // 
            // lLenghtOfMax
            // 
            this.lLenghtOfMax.Location = new System.Drawing.Point(466, 281);
            this.lLenghtOfMax.Name = "lLenghtOfMax";
            this.lLenghtOfMax.Size = new System.Drawing.Size(284, 24);
            this.lLenghtOfMax.TabIndex = 8;
            this.lLenghtOfMax.Text = "Текст обновления: 0 из 400";
            this.lLenghtOfMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 598);
            this.Controls.Add(this.lLenghtOfMax);
            this.Controls.Add(this.bNewFunc);
            this.Controls.Add(this.bOpt);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.bHotFix);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tWhatNews);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Менеджер обновлений";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rRelease;
        private System.Windows.Forms.RadioButton rBeta;
        private System.Windows.Forms.RadioButton rAlpha;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cCookContent;
        private System.Windows.Forms.CheckBox cBuildServer;
        private System.Windows.Forms.CheckBox cBuildGame;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cAIServerVersion;
        private System.Windows.Forms.CheckBox cDeprecateServerVersions;
        private System.Windows.Forms.CheckBox cDeprecateGameVersions;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton rDev;
        private System.Windows.Forms.CheckBox cPublishGooglePlay;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RichTextBox tWhatNews;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button bNewFunc;
        private System.Windows.Forms.Button bOpt;
        private System.Windows.Forms.Button bHotFix;
        private System.Windows.Forms.Label lLenghtOfMax;
        private System.Windows.Forms.CheckBox cGooglePlayGameVersion;
    }
}

