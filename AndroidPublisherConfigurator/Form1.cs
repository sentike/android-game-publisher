﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AndroidPublisherConfigurator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //======================================
            var sb = new StringBuilder(8192);

            //======================================
            string BranchName = string.Empty;

            //======================================
            if (rAlpha.Checked)
            {
                sb.Append(BranchName = "Alpha ");
            }
            else if (rBeta.Checked)
            {
                sb.Append(BranchName = "Beta ");
            }
            else if (rRelease.Checked)
            {
                sb.Append(BranchName = "Production ");
            }
            else
            {
                sb.Append(BranchName = "Internal ");
            }

            //======================================
            if (cPublishGooglePlay.Checked && cPublishGooglePlay.Enabled)
            {
                sb.Append("--IsAllowPublishToGooglePlay ".ToLowerInvariant());
            }

            //======================================
            if (cBuildGame.Checked == false)
            {
                sb.Append("--SkipGameDeploy ".ToLowerInvariant());
            }

            //======================================
            if (cBuildServer.Checked == false)
            {
                sb.Append("--SkipServerDeploy ".ToLowerInvariant());
            }

            //======================================
            if (cCookContent.Checked == false)
            {
                sb.Append("--SkipContentDeploy ".ToLowerInvariant());
            }

            //======================================
            if (cDeprecateGameVersions.Checked && cDeprecateGameVersions.Enabled)
            {
                sb.Append("--DeprecateGameVersions ".ToLowerInvariant());
            }

            //======================================
            if (cDeprecateServerVersions.Checked && cDeprecateServerVersions.Enabled)
            {
                sb.Append("--DeprecateServerVersions ".ToLowerInvariant());
            }

            //======================================
            if (cAIServerVersion.Checked && cAIServerVersion.Enabled)
            {
                sb.Append("--IsAllowServerVersionIncrement ".ToLowerInvariant());
            }

            //======================================
            if (cGooglePlayGameVersion.Checked && cGooglePlayGameVersion.Enabled)
            {
                sb.Append("--IsAllowUpdateGoogleVersionManifest ".ToLowerInvariant());
            }

            //======================================
            var JsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(Messages);
            var BaseMessage = Base64Encode(JsonMessage);

            //======================================
            sb.Append($"--Message=".ToLowerInvariant());
            sb.Append($"\"{BaseMessage}\"");

            //======================================
            //if (Process.GetProcessesByName("Android Game Publisher.exe").Any())
            //{
            //    lDateTime.Text = "Сборка уже запущена или уже завершена";
            //}
            //else
            {
                //button1.Enabled = false;
                button1.Text = $"{BranchName} | {DateTime.Now.ToShortTimeString()}";
                Process.Start("Android Game Publisher.exe", sb.ToString());
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private void cBuildGame_CheckedChanged(object sender, EventArgs e)
        {
            cAIServerVersion.Enabled = cBuildGame.Checked;
            cDeprecateGameVersions.Enabled = cBuildGame.Checked;
            cPublishGooglePlay.Enabled = cBuildGame.Checked;
        }

        private void cBuildServer_CheckedChanged(object sender, EventArgs e)
        {
            cDeprecateServerVersions.Enabled = cBuildServer.Checked;
            cAIServerVersion.Enabled = cBuildServer.Checked;
        }

        public Languages CurrentLanguage => (Languages)comboBox1.SelectedIndex;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Messages.TryGetValue(CurrentLanguage, out var msg))
            {
                tWhatNews.Text = msg;
            }
        }

        private void bHotFix_Click(object sender, EventArgs e)
        {
            Messages[Languages.Russian] = "В данном обновлении было исправлено множество ошибок";
            Messages[Languages.English] = "In this update, many bugs were fixed";
            Messages[Languages.Germany] = "In diesem Update wurden viele Fehler behoben";
            tWhatNews.Text = Messages[CurrentLanguage];
        }

        private void bOpt_Click(object sender, EventArgs e)
        {
            Messages[Languages.Russian] = "В данном обновлении были произведены улучшения по оптимизации игры. \nУвеличилась производительность на слабых устройствах. \nУлучшено качество картинки на более производительных устройствах.";
            Messages[Languages.English] = "In this update, improvements were made to optimize the game. \nUp performance increased on weak devices. \nImproved picture quality on more productive devices.";
            Messages[Languages.Germany] = "In diesem Update wurden Verbesserungen vorgenommen, um das Spiel zu optimieren. \nDie Leistung wurde auf schwachen Geräten erhöht. \nVerbesserte Bildqualität auf produktiveren Geräten.";
            tWhatNews.Text = Messages[CurrentLanguage];
        }

        private void bNewFunc_Click(object sender, EventArgs e)
        {
            Messages[Languages.Russian] = "В данном обновлении был добавлен новый функционал в игру!";
            Messages[Languages.English] = "In this update, a new functionality has been added to the game!";
            Messages[Languages.Germany] = "In diesem Update wurde dem Spiel eine neue Funktionalität hinzugefügt!";
            tWhatNews.Text = Messages[CurrentLanguage];
        }

        public Dictionary<Languages, string> Messages { get; set; } = new Dictionary<Languages, string>
        {
            { Languages.Russian, string.Empty },
            { Languages.English, string.Empty },
            { Languages.Germany, string.Empty },
        };

        private void tWhatNews_TextChanged(object sender, EventArgs e)
        {
            Messages[CurrentLanguage] = tWhatNews.Text;
            lLenghtOfMax.Text = $"Текст обновления: {tWhatNews.Text.Length} из 400";
            if(tWhatNews.Text.Length > 430)
            {
                tWhatNews.ForeColor = Color.Red;
                lLenghtOfMax.ForeColor = Color.Red;
            }
            else
            {
                tWhatNews.ForeColor = Color.Black;
                lLenghtOfMax.ForeColor = Color.Black;
            }
        }


    }

    public enum Languages
    {
        Russian,
        English,
        Germany,
    }

}
